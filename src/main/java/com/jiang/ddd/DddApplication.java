package com.jiang.ddd;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy(exposeProxy = true, proxyTargetClass = true)
@MapperScan({ "com.jiang.ddd.business.mapper", "com.jiang.ddd.aop.audit.mapper" })
public class DddApplication {

	public static boolean isInited = false;

	public static void main(String[] args) {
		SpringApplication.run(DddApplication.class, args);
		isInited = true;
	}

}
