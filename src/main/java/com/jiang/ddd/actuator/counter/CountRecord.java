package com.jiang.ddd.actuator.counter;

import java.lang.annotation.*;

/**
 * 计数元数据注解
 *
 * @author jiang.li
 * @since 2021/8/22
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CountRecord {

	// 名称
	String name();

	// 描述
	String description() default "";

	// 标签
	String[] tags() default {};

}
