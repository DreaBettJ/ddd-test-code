package com.jiang.ddd.actuator.counter;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Metrics;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 计数元数据切面
 *
 * @author jiang.li
 * @since 2021/8/22
 */
@Aspect
@Component
public class CounterAOP {

	public static Map<String, Counter> nameCounterMap = new HashMap<>();

	@Before("@annotation(countRecord)")
	public void count(CountRecord countRecord) {
		// 不存在则新建
		String dateStr = LocalDate.now().format(DateTimeFormatter.ISO_DATE);
		String hashKeyStr = String.format("%s,%s,%s", String.join(",", countRecord.tags()), countRecord.name(),
				dateStr);
		Counter counter = nameCounterMap.get(hashKeyStr);

		if (Objects.isNull(counter)) {
			counter = Counter.builder(countRecord.name()) // 指标的名字
					.description(countRecord.description()) // 指标的描述
					.baseUnit("次") // 指标的单位
					.tags(countRecord.tags()) // 标签
					.tag("date", dateStr).register(Metrics.globalRegistry); // 注册到全局
																			// MeterRegistry
																			// 指标注册表
			nameCounterMap.put(hashKeyStr, counter);
		}
		// 增加
		counter.increment();
	}

}
