package com.jiang.ddd.aop.advice;

import com.jiang.ddd.common.dto.CommonResult;
import com.jiang.ddd.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 业务异常校验
 *
 * @author jiang.li
 * @since 2021/12/12
 */
@RestControllerAdvice
@Slf4j
@Order(1)
public class BusinessExceptionHandler {

	/**
	 * 校验异常
	 */
	@ExceptionHandler(value = BusinessException.class)
	public CommonResult<Void> constraintViolationExceptionHandler(BusinessException ex) {
		return CommonResult.error(ex.getCode());
	}

}
