package com.jiang.ddd.aop.advice;

import com.jiang.ddd.common.code.SystemCodeEnum;
import com.jiang.ddd.common.dto.CommonResult;
import com.jiang.ddd.tool.I18nTool;
import com.jiang.ddd.tool.StringTool;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.connector.Response;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.NestedRuntimeException;
import org.springframework.core.annotation.Order;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Arrays;
import java.util.Objects;

/**
 * 全局系统异常处理
 *
 * @author jiang.li
 * @since 2021/12/12
 */
@Slf4j
@RestControllerAdvice
@Order(10)
public class SystemGlobalExceptionHandler {

	/**
	 * 校验异常
	 */
	@ExceptionHandler(value = ConstraintViolationException.class)
	public CommonResult<Void> constraintViolationExceptionHandler(ConstraintViolationException ex) {
		log.debug("[constraintViolationExceptionHandler]", ex);
		StringBuilder messageBuilder = new StringBuilder();
		for (ConstraintViolation<?> constraintViolation : ex.getConstraintViolations()) {
			String propertyPath = constraintViolation.getPropertyPath().toString();
			String property = StringTool.getAfterBack(propertyPath, ".");
			messageBuilder.append(String.format("%s - %s;", property, constraintViolation.getMessage()));
		}

		String message = I18nTool.getMessage(SystemCodeEnum.INVALID_REQUEST_PARAM_ERROR.getCode()) + ":"
				+ messageBuilder;
		return CommonResult.error(SystemCodeEnum.INVALID_REQUEST_PARAM_ERROR.getCode(), message);
	}

	/**
	 * 校验异常
	 */
	@ExceptionHandler(value = BindException.class)
	public CommonResult<Void> bindExceptionHandler(BindException ex) {
		log.debug("[bindExceptionHandler]", ex);
		// 拼接错误
		StringBuilder detailMessage = new StringBuilder();
		// field 异常
		for (FieldError fieldError : ex.getFieldErrors()) {
			detailMessage.append(String.format("%s - %s;", fieldError.getField(), fieldError.getDefaultMessage()));
		}
		// 包装 CommonResult 结果
		return CommonResult.error(SystemCodeEnum.INVALID_REQUEST_PARAM_ERROR.getCode(),
				I18nTool.getMessage(SystemCodeEnum.INVALID_REQUEST_PARAM_ERROR.getCode()) + ":"
						+ detailMessage);
	}

	/**
	 * servlet 和 mvc 的异常处理，优化前端提示
	 */
	@ExceptionHandler(value = { ServletException.class, NestedRuntimeException.class })
	public CommonResult<Void> servletExceptionHandler(HttpServletResponse response, Exception exception) {
		log.error("[Servlet Exception]", exception);
		String detailMessage = null;
		SystemCodeEnum systemCodeEnum = null;

		// 处理各种异常，方便前端
		if (exception instanceof ServletException) {
			ServletException servletException = (ServletException) exception;
			if (servletException instanceof MissingServletRequestParameterException) {
				// 参数缺失
				systemCodeEnum = SystemCodeEnum.MISSING_REQUEST_PARAM_ERROR;
				detailMessage = ((MissingServletRequestParameterException) servletException).getParameterName();
			}
			else if (servletException instanceof HttpRequestMethodNotSupportedException) {
				// 方法不支持
				systemCodeEnum = SystemCodeEnum.METHOD_NOT_SUPPORT;
				detailMessage = String.format("支持的方式 %s", Arrays
						.toString(((HttpRequestMethodNotSupportedException) servletException).getSupportedMethods()));
			}
		}
		else {
			NestedRuntimeException nestedRuntimeException = (NestedRuntimeException) exception;
			if (nestedRuntimeException instanceof HttpMessageNotReadableException) {
				// 请求体不可读
				systemCodeEnum = SystemCodeEnum.BAD_REQUEST_BODY;
				detailMessage = nestedRuntimeException.getMessage();
			}
			else if (nestedRuntimeException instanceof MethodArgumentTypeMismatchException) {
				// 请求参数不匹配
				systemCodeEnum = SystemCodeEnum.INVALID_REQUEST_PARAM_ERROR;
				MethodArgumentTypeMismatchException methodArgumentTypeMismatchException = (MethodArgumentTypeMismatchException) nestedRuntimeException;
				detailMessage = String.format("参数 - %s，详情 - %s", methodArgumentTypeMismatchException.getName(),
						methodArgumentTypeMismatchException.getMessage());
			}
		}

		// 托底处理
		if (Objects.isNull(systemCodeEnum)) {
			systemCodeEnum = SystemCodeEnum.SERVLET_ERROR;
			detailMessage = exception.getMessage();
		}

		// 设置状态码
		response.setStatus(Response.SC_BAD_REQUEST);

		// 拼接
		String message;
		if (StringUtils.isNotEmpty(detailMessage)) {
			message = String.format("%s:%s", I18nTool.getMessage(systemCodeEnum.getCode()), detailMessage);
		}
		else {
			message = I18nTool.getMessage(systemCodeEnum.getCode());
		}

		return CommonResult.error(systemCodeEnum.getCode(), message);
	}

	/**
	 * 处理其它 Exception 异常
	 */
	@ExceptionHandler(value = Exception.class)
	public CommonResult<Void> exceptionHandler(Exception e, HttpServletResponse response) {
		// 记录异常日志
		log.error("[exceptionHandler]", e);
		// 设置装填码
		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		// 返回 ERROR CommonResult
		return CommonResult.error(SystemCodeEnum.SYS_ERROR.getCode());
	}

}