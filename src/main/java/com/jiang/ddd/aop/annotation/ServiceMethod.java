package com.jiang.ddd.aop.annotation;

import java.lang.annotation.*;

/**
 * 用于标注model中的服务方法
 * 
 * @author jiang.li
 * @since 2021/10/2
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ServiceMethod {

}
