package com.jiang.ddd.aop.aspect;

import com.jiang.ddd.context.ControllerAnnoMarkContext;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * 存储方法注解的 aop
 *
 * @author jiang.li
 * @since 2021/12/4
 */
@Component
@Aspect
public class ControllerAnnoStoreAspect {

	@Resource
	private ControllerAnnoMarkContext annoMarkContext;

	@Around("(@within(org.springframework.stereotype.Controller) || "
			+ "@within(org.springframework.web.bind.annotation.RestController))")
	public Object aroundApi(ProceedingJoinPoint point) throws Throwable {
		// 获取方法上的注解
		if (point.getSignature() instanceof MethodSignature) {
			MethodSignature methodSignature = (MethodSignature) point.getSignature();
			Method method = methodSignature.getMethod();
			Annotation[] annotations = method.getAnnotations();
			for (Annotation annotation : annotations) {
				annoMarkContext.set(annotation.annotationType(), annotation);
			}
		}
		return point.proceed();
	}

}
