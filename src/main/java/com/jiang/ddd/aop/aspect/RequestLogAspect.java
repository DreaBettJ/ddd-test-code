package com.jiang.ddd.aop.aspect;

import cn.hutool.json.JSONUtil;
import com.jiang.ddd.tool.AOPTool;
import com.jiang.ddd.tool.ReflectInfoTool;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * Spring boot 控制器 请求日志，方便代码调试
 *
 * @author L.cm
 */
@Slf4j
@Aspect
@Configuration(proxyBeanMethods = false)
@Order(1)
public class RequestLogAspect {

	@Resource
	ReflectInfoTool reflectInfoTool;

	/**
	 * AOP 环切 控制器 R 返回值
	 * @param point JoinPoint
	 * @return Object
	 * @throws Throwable 异常
	 */
	@Around("(@within(org.springframework.stereotype.Controller) || "
			+ "@within(org.springframework.web.bind.annotation.RestController))")
	public Object aroundApi(ProceedingJoinPoint point) throws Throwable {
		// 请求参数处理xxxxxxxxxxxxxxxxxxxx
		MethodSignature ms = (MethodSignature) point.getSignature();
		int requestBodyIndex = reflectInfoTool.indexParameterByAnnotation(ms.getMethod(), RequestBody.class);

		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		String requestURL = request.getRequestURL().toString();
		String queryString = request.getQueryString();
		if (Objects.nonNull(queryString)) {
			requestURL = String.format("%s?%s", requestURL, queryString);
		}
		String requestMethod = request.getMethod();

		// 构建成一条长 日志，避免并发下日志错乱
		StringBuilder beforeReqLog = new StringBuilder(300);
		// 日志参数
		List<Object> beforeReqArgs = new ArrayList<>();
		beforeReqLog.append("================  Request Start  ================\n");
		// 打印路由
		beforeReqLog.append("===> {}: {} \n");
		beforeReqArgs.add(requestMethod);
		beforeReqArgs.add(requestURL);
		// 请求参数
		if (!Objects.equals(requestBodyIndex, -1)) {
			beforeReqLog.append("===RequestBody===  \n {}\n");
			beforeReqArgs.add(JSONUtil.formatJsonStr(JSONUtil.toJsonStr(point.getArgs()[requestBodyIndex])));
		}
		// 打印请求头
		Enumeration<String> headers = request.getHeaderNames();
		while (headers.hasMoreElements()) {
			String headerName = headers.nextElement();
			String headerValue = request.getHeader(headerName);
			beforeReqLog.append("===Headers===  {} : {}\n");
			beforeReqArgs.add(headerName);
			beforeReqArgs.add(headerValue);
		}
		beforeReqLog.append("================  Request End   ================\n");
		// 打印执行时间
		long startNs = System.nanoTime();
		log.info(beforeReqLog.toString(), beforeReqArgs.toArray());
		// aop 执行后的日志
		StringBuilder afterReqLog = new StringBuilder(200);
		// 日志参数
		List<Object> afterReqArgs = new ArrayList<>();
		afterReqLog.append("================  Response Start  ================\n");
		try {
			Object result = point.proceed();
			// 打印返回结构体
			afterReqLog.append("===Result===  {}\n");
			afterReqArgs.add(AOPTool.toShowString(result));
			return result;
		}
		finally {
			long tookMs = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startNs);
			afterReqLog.append("<=== {}: {} ({} ms)\n");
			afterReqArgs.add(requestMethod);
			afterReqArgs.add(requestURL);
			afterReqArgs.add(tookMs);
			afterReqLog.append("================  Response End   ================\n");
			log.info(afterReqLog.toString(), afterReqArgs.toArray());
		}
	}

}
