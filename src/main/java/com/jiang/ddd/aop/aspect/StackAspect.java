package com.jiang.ddd.aop.aspect;

import com.jiang.ddd.constant.AppConstants;
import com.jiang.ddd.tool.AOPTool;
import com.jiang.ddd.tool.StackTool;
import com.jiang.ddd.tool.WatchTool;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 本地调试方法调用栈
 *
 * @author jiang.li
 * @since 2021/8/25
 */
@Aspect
@Component
@Slf4j
public class StackAspect {

	@Resource
	Executor logPool;

	@Pointcut("within(com.jiang.ddd.business..*)")
	private void packagePointCut() {
	}

	@Pointcut("this(com.baomidou.mybatisplus.extension.service.IService) || this(com.baomidou.mybatisplus.core.mapper.BaseMapper)")
	private void basicMethod() {
	}

	@Pointcut("this(com.jiang.ddd.business.base.BasicModel)")
	private void matchModel() {
	}

	@Pointcut("matchModel() && @annotation(com.jiang.ddd.aop.annotation.ServiceMethod)")
	private void modelServiceMethod() {
	}

	@Pointcut("!within(com.jiang.ddd.aop..*)")
	private void noAop() {
	}

	private static final ThreadLocal<Integer> level = InheritableThreadLocal.withInitial(() -> 0);

	private static final ThreadLocal<WatchTool.WatchStop> watchStopThreadLocal = InheritableThreadLocal.withInitial(WatchTool.WatchStop::new);

	@Around("packagePointCut() &&!matchModel() || modelServiceMethod()||basicMethod() && noAop()")
	public Object stackLogAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
		// 层级递进
		level.set(level.get() + 1);

		// 类名
		String className = joinPoint.getSignature().getDeclaringTypeName();
		String methodName = joinPoint.getSignature().getName();

		// 请求前日志
		AtomicLong beforeTime = new AtomicLong();
		logPool.execute(() -> beforeTime.set(beforeProcessLog(joinPoint, className, methodName)));
		watchStopThreadLocal.get().start(className + "#" + methodName);

		// 请求结果
		Object proceed = joinPoint.proceed();
		// 请求后日志
		logPool.execute(() -> endProcessLog(className, methodName, beforeTime.get(), proceed));
		watchStopThreadLocal.get().stop();

		level.set(level.get() - 1);

		// 最后打印整体耗时分析
		if (Objects.equals(level.get(),0)){
			log.info(watchStopThreadLocal.get().prettyPrint());
		}

		return proceed;
	}

	/**
	 * 处理方法结束日志
	 * @param className 类名
	 * @param methodName 方法名
	 * @param beforeTime 开始时间
	 * @param proceed 方法返回结果
	 */
	private void endProcessLog(String className, String methodName, long beforeTime, Object proceed) {
		String methodStr = String.format("%s#%s  ", className, methodName);
		StringBuilder endStringBuilder = new StringBuilder();
		// 添加前缀
		endStringBuilder.append("<");
		for (int i = 0; i < level.get(); i++) {
			endStringBuilder.append("==============");
		}
		endStringBuilder.append("  ");
		endStringBuilder.append("[ end ] ");
		endStringBuilder.append(methodStr);

		// 请求结果
		endStringBuilder.append(String.format("|  请求结果: %s", AOPTool.toShowString(proceed)));

		// 请求结果
		StackTraceElement lastStackInPackages = StackTool.getLastStack(new Throwable(), (stackTraceElement) -> {
			String nowClassName = stackTraceElement.getClassName();
			String nowMethodName = stackTraceElement.getMethodName();
			boolean isApplicationCode = nowClassName.contains(AppConstants.Package.ROOT);
			boolean isNotStackAspect = !nowClassName.contains(this.getClass().getName());
			boolean isSelf = nowClassName.contains(className) && nowMethodName.contains(methodName);
			return isApplicationCode && isNotStackAspect && !isSelf;
		});
		endStringBuilder.append(String.format("|  调用方法: %s#%s  ", lastStackInPackages.getClassName(),
				lastStackInPackages.getMethodName()));

		// 结果
		long endTime = System.currentTimeMillis();
		endStringBuilder.append(String.format("|  持续时间：%sms  ", endTime - beforeTime));
		// 方法结束
		log.info(endStringBuilder.toString());
	}

	private long beforeProcessLog(ProceedingJoinPoint joinPoint, String className, String methodName) {
		String methodStr = String.format("%s#%s  ", className, methodName);
		StringBuilder startStringBuilder = new StringBuilder();

		// 前缀
		for (int i = 0; i < level.get(); i++) {
			startStringBuilder.append("==============");
		}
		startStringBuilder.append(">  ");
		startStringBuilder.append("[start] ");
		startStringBuilder.append(methodStr);

		// 请求参数
		Map<String, Object> methodParam = AOPTool.getCanJsonParameters(joinPoint);
		startStringBuilder.append(String.format("|  请求参数: %s   ", AOPTool.toShowString(methodParam)));
		// 方法开始
		log.info(startStringBuilder.toString());

		// 执行之前时间
		return System.currentTimeMillis();
	}

}
