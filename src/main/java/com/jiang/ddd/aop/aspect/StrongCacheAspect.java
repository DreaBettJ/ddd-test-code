package com.jiang.ddd.aop.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * 缓存切面，用于处理反射读取信息的缓存
 *
 * @author jiang.li
 * @since 2021/12/12
 */
@Aspect
@Component
public class StrongCacheAspect {

	public static Map<String, Map<Object[], Object>> cacheMap = new HashMap<>();

	@Around("within(com.jiang.ddd.tool.ReflectInfoTool)")
	public Object process(ProceedingJoinPoint jp) throws Throwable {
		String methodName = jp.getSignature().getName();
		// 命中 method 缓存
		if (cacheMap.containsKey(methodName)) {
			Map<Object[], Object> objectMap = cacheMap.get(methodName);
			Object[] args = jp.getArgs();
			Set<Map.Entry<Object[], Object>> entries = objectMap.entrySet();
			for (Map.Entry<Object[], Object> entry : entries) {
				Object[] cacheArgs = entry.getKey();
				if (!Objects.equals(cacheArgs.length, args.length)) {
					continue;
				}
				if (allEqual(args, cacheArgs)) {
					return entry.getValue();
				}
			}
		}
		return noCacheProcess(jp);
	}

	/**
	 * 处理无缓存结果
	 */
	private Object noCacheProcess(ProceedingJoinPoint jp) throws Throwable {
		Object proceed = jp.proceed();
		Map<Object[], Object> argAndResultMap = new HashMap<>();
		argAndResultMap.put(jp.getArgs(), proceed);
		cacheMap.put(jp.getSignature().getName(), argAndResultMap);
		return proceed;
	}

	/**
	 * 参数全部命中
	 */
	private boolean allEqual(Object[] args, Object[] cacheArgs) {
		boolean result = true;
		for (int i = 0; i < args.length; i++) {
			Object arg = args[i];
			Object cacheArg = cacheArgs[i];
			if (!Objects.equals(arg, cacheArg)) {
				result = false;
			}
		}
		return result;
	}

}
