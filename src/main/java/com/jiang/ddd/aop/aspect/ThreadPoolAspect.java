package com.jiang.ddd.aop.aspect;

import com.jiang.ddd.context.BasicContext;
import com.jiang.ddd.tool.AOPTool;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;

/**
 * 线程池异步调用切面
 *
 * @author jiang.li
 * @since 2021/8/25
 */
@Aspect
@Component
@Slf4j
public class ThreadPoolAspect {

	@Resource
	List<? extends BasicContext> contexts;

	@Pointcut("execution(* java.util.concurrent.ExecutorService.*(..))")
	private void poolExec() {
	}

	@Around("poolExec()")
	public Object stackLogAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
		// 将context中内容存储为本地对象
		List<Object> localContentList = new ArrayList<>();
		contexts.forEach(basicContext -> localContentList.add(basicContext.get()));

		// 处理Runnable
		Runnable runnable = AOPTool.getRequestParamByClazz(joinPoint, Runnable.class, 0);
		if (Objects.nonNull(runnable)) {
			Runnable finalRunnable = runnable;
			runnable = () -> {
				contextTransfer(localContentList);
				finalRunnable.run();
			};
			AOPTool.replaceRequestParamByClazz(joinPoint, Runnable.class, 0, runnable);
		}

		// 处理Callable
		Callable callable = AOPTool.getRequestParamByClazz(joinPoint, Callable.class, 0);
		if (Objects.nonNull(runnable)) {
			Callable finalCallable = callable;
			callable = () -> {
				contextTransfer(localContentList);
				return finalCallable.call();
			};
			AOPTool.replaceRequestParamByClazz(joinPoint, Callable.class, 0, callable);
		}

		// 请求结果
		return joinPoint.proceed();
	}

	/**
	 * 上下文转移
	 * @param localContentList 本地上下文副本
	 */
	private void contextTransfer(List<Object> localContentList) {
		for (int i = 0; i < contexts.size(); i++) {
			if (Objects.nonNull(localContentList.get(i)) && Objects.nonNull(contexts.get(i))) {
				contexts.get(i).set(localContentList.get(i));
			}
		}
	}

}
