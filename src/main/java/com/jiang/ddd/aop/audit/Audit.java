package com.jiang.ddd.aop.audit;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 审计注解
 *
 * @author jiang.li
 * @version v1.0.0
 * @since 2021/4/19
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Audit {

	// 别名
	String alias() default "";

	// 排序
	int index() default Integer.MAX_VALUE;

	// 展示规则
	Class<? extends ShowValueProcessor> showValueProcessor() default DefaultShowValueProcessor.class;

	// 是否固定
	boolean isFixed() default false;

	// 是否忽略
	boolean ignore() default false;

}