package com.jiang.ddd.aop.audit;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jiang.ddd.business.base.BasicEntity;
import com.jiang.ddd.tool.AOPTool;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.Executor;

/**
 * 尝试审计自动化
 *
 * @author jiang.li
 * @since 2021/11/4
 */
@Aspect
@Slf4j
@Transactional
public class AuditAspect {

	@Resource
	Executor logPool;

	@Resource
	AuditUtils auditUtils;

	/**
	 * 用于记录调用者
	 */
	private volatile Throwable throwable;

	/**
	 * 排除自己的mapper
	 */
	@Pointcut("bean(auditRecordMapper)")
	private void selfMapper() {
	}

	@Pointcut("execution(* com.baomidou.mybatisplus.core.mapper.BaseMapper.update*(..)) && !selfMapper()")
	private void updateCut() {
	}

	@Around("updateCut()")
	public Object updateAudit(ProceedingJoinPoint joinPoint) throws Throwable {
		throwable = new Throwable();
		logPool.execute(() -> processUpdateLog(joinPoint));
		return joinPoint.proceed();
	}

	@Pointcut("execution(* com.baomidou.mybatisplus.core.mapper.BaseMapper.insert*(..)) && !selfMapper()")
	private void insertCut() {
	}

	@Around("insertCut()")
	public Object insertCut(ProceedingJoinPoint joinPoint) throws Throwable {
		throwable = new Throwable();
		Object proceed = joinPoint.proceed();
		// 插入之后对象中才有id
		logPool.execute(() -> processInsertLog(joinPoint));
		return proceed;
	}

	@Pointcut("execution(* com.baomidou.mybatisplus.core.mapper.BaseMapper.delete*(..)) && !selfMapper()")
	private void deleteCut() {
	}

	@Around("deleteCut()")
	public Object deleteCut(ProceedingJoinPoint joinPoint) throws Throwable {
		throwable = new Throwable();
		logPool.execute(() -> processDeleteLog(joinPoint));
		return joinPoint.proceed();
	}

	/**
	 * 处理删除日志
	 */
	private void processDeleteLog(ProceedingJoinPoint joinPoint) {
		// 方法名
		BaseMapper<BasicEntity> mapper = (BaseMapper<BasicEntity>) joinPoint.getTarget();
		BasicEntity targetEntity = AOPTool.getRequestParamByClazz(joinPoint, BasicEntity.class, 0);

		// 获取之前实体
		List<BasicEntity> beforeEntities = getBeforeEntities(joinPoint, mapper);

		// 记录
		beforeEntities.forEach(beforeEntity -> auditUtils.processLog(AuditUtils.AuditType.DELETE, beforeEntity,
				targetEntity, throwable));
	}

	/**
	 * 处理实体更新日志
	 */
	private void processUpdateLog(ProceedingJoinPoint joinPoint) {
		// 获取之前实体
		BaseMapper<BasicEntity> mapper = (BaseMapper<BasicEntity>) joinPoint.getTarget();
		BasicEntity targetEntity = AOPTool.getRequestParamByClazz(joinPoint, BasicEntity.class, 0);
		List<BasicEntity> beforeEntities = getBeforeEntities(joinPoint, mapper);
		// 记录
		beforeEntities.forEach(beforeEntity -> auditUtils.processLog(AuditUtils.AuditType.UPDATE, beforeEntity,
				targetEntity, throwable));
	}

	/**
	 * 处理实体新增日志
	 */
	private void processInsertLog(ProceedingJoinPoint joinPoint) {
		// 方法名
		BasicEntity targetEntity = AOPTool.getRequestParamByClazz(joinPoint, BasicEntity.class, 0);
		auditUtils.processLog(AuditUtils.AuditType.INSERT, null, targetEntity, throwable);
	}

	/**
	 * 获取之前所有实体对象,基于mybatis plus 的base方法
	 */
	private List<BasicEntity> getBeforeEntities(ProceedingJoinPoint joinPoint, BaseMapper<BasicEntity> mapper) {
		// 存储之前对象
		List<BasicEntity> beforeEntities = new ArrayList<>();
		// wrapper对象
		Serializable id = (Serializable) AOPTool.getRequestParamByName(joinPoint, "id");
		// 更新byId传参有点特别
		BasicEntity basicEntity = AOPTool.getRequestParamByClazz(joinPoint, BasicEntity.class, 0);
		if (Objects.isNull(id) && Objects.nonNull(basicEntity)) {
			id = basicEntity.getId();
		}
		Map map = AOPTool.getRequestParamByClazz(joinPoint, Map.class, 0);
		Wrapper wrapper = AOPTool.getRequestParamByClazz(joinPoint, Wrapper.class, 0);
		Collection byBatchId = AOPTool.getRequestParamByClazz(joinPoint, Collection.class, 0);
		if (Objects.nonNull(id)) {
			beforeEntities.add(mapper.selectById(id));
		}
		else if (Objects.nonNull(map)) {
			beforeEntities.addAll(mapper.selectByMap(map));
		}
		else if (Objects.nonNull(wrapper)) {
			beforeEntities.addAll(mapper.selectList(wrapper));
		}
		else if (Objects.nonNull(byBatchId)) {
			beforeEntities.addAll(mapper.selectBatchIds(byBatchId));
		}
		return beforeEntities;
	}

}
