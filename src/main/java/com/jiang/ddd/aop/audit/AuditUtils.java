package com.jiang.ddd.aop.audit;

import com.jiang.ddd.aop.audit.domain.AuditRecord;
import com.jiang.ddd.aop.audit.mapper.AuditRecordMapper;
import com.jiang.ddd.business.base.BasicEntity;
import com.jiang.ddd.constant.AppConstants;
import com.jiang.ddd.context.MDCContext;
import com.jiang.ddd.tool.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 审计工具类
 *
 * @author jiang.li
 * @version v1.0.0
 * @since 2021/4/20
 */
@Component
public class AuditUtils {

	@Resource
	AutowireCapableBeanFactory beanFactory;

	@Resource
	MDCContext mdcContext;

	@Resource
	AuditRecordMapper auditRecordMapper;

	@Resource
	ReflectInfoTool reflectInfoTool;

	@Resource
	ReflectInvokeTool reflectInvokeTool;

	// entityClass -> [field -> audit注解,。。。]
	private Map<Class<?>, List<Map.Entry<Field, Audit>>> classInfos;

	/**
	 * 固定排除字段
	 */
	private final List<String> excludeFields = Arrays.asList("serialVersionUID");

	@PostConstruct
	public void init() {
		List<Class<?>> classList = reflectInfoTool.getClassInPackage(
				Collections.singletonList(new ReflectInfoTool.SubClassFilter<>(BasicEntity.class)),
				Collections.emptyList(), AppConstants.Package.ROOT);

		classInfos = classList.stream().collect(Collectors.toMap(Function.identity(),
				clazz -> reflectInfoTool.getAllField(clazz).stream().map(field -> {
					Audit audit = NullTool.of(() -> field.getAnnotation(Audit.class)).orElse(defaultAudit());
					return new AbstractMap.SimpleImmutableEntry<>(field, audit);
				}).sorted((beforeEntry, afterEntry) -> {
					Audit beforeAudit = beforeEntry.getValue();
					Audit afterAudit = afterEntry.getValue();
					return beforeAudit.index() - afterAudit.index();
				}).collect(Collectors.toList())));
	}

	/**
	 * 获取审计字符串
	 */
	public String getAuditString(Class<? extends BasicEntity> entityClass, BasicEntity beforeObject,
			BasicEntity afterObject, AuditType type) {
		List<Map.Entry<Field, Audit>> entries = classInfos.get(entityClass);
		List<String> resultStrList = new ArrayList<>();
		for (Map.Entry<Field, Audit> fieldAuditEntry : entries) {
			Field field = fieldAuditEntry.getKey();
			Audit audit = fieldAuditEntry.getValue();

			// 跳过忽略
			if (audit.ignore() || excludeFields.contains(field.getName())) {
				continue;
			}

			// 获取别称
			String alias;
			if (StringUtils.isEmpty(audit.alias())) {
				alias = field.getName();
			}
			else {
				alias = audit.alias();
			}

			// 获取显示值
			String beforeShowValue = getShowValue(beforeObject, field, audit);
			String afterShowValue = getShowValue(afterObject, field, audit);

			// 插入内容
			switch (type) {
			case INSERT:
				resultStrList.add(String.format("%s:%s", alias, afterShowValue));
				break;
			case DELETE:
				resultStrList.add(String.format("%s:%s", alias, beforeShowValue));
				break;
			case UPDATE:
				if (!Objects.equals(beforeShowValue, afterShowValue) && StringUtils.isNotEmpty(afterShowValue)) {
					resultStrList.add(String.format("%s:%s --> %s", alias, beforeShowValue, afterShowValue));
				}
				break;
			}
		}
		return String.join("  ", resultStrList);
	}

	/**
	 * 提过默认注解实例
	 */
	private Audit defaultAudit() {
		return new Audit() {

			@Override
			public Class<? extends Annotation> annotationType() {
				return Audit.class;
			}

			@Override
			public String alias() {
				return "";
			}

			@Override
			public int index() {
				return Integer.MAX_VALUE;
			}

			@Override
			public Class<? extends ShowValueProcessor> showValueProcessor() {
				return DefaultShowValueProcessor.class;
			}

			@Override
			public boolean isFixed() {
				return false;
			}

			@Override
			public boolean ignore() {
				return false;
			}
		};
	}

	/**
	 * 实际日志处理
	 * @param auditType 操作类型
	 * @param beforeEntity 之前的目标实体
	 * @param targetEntity 替换的目标实体
	 */
	public void processLog(AuditUtils.AuditType auditType, BasicEntity beforeEntity, BasicEntity targetEntity,
			Throwable throwable) {
		Class<? extends BasicEntity> aClass;
		Long id;
		if (Objects.nonNull(beforeEntity)) {
			aClass = beforeEntity.getClass();
			id = beforeEntity.getId();
		}
		else {
			aClass = targetEntity.getClass();
			id = targetEntity.getId();
		}
		String auditString = this.getAuditString(aClass, beforeEntity, targetEntity, auditType);
		if (StringUtils.isNotEmpty(auditString) && !Objects.equals(aClass, AuditRecord.class)) {
			StackTraceElement lastStackInPackages = StackTool.getLastStackInPackages(throwable,
					AppConstants.Package.BUSINESS);
			AuditRecord record = AuditRecord.builder().tableName(StringTool.toTableNameForm(aClass.getSimpleName()))
					.auditMessage(auditString).tableId(id).traceId(mdcContext.get().get(AppConstants.AOP.TRANCE_ID_KEY))
					.operateType(auditType.getName())
					.operateMethod(String.format("%s#%s", lastStackInPackages.getClassName(),
							lastStackInPackages.getMethodName()))
					.operateMethodLineNumber(lastStackInPackages.getLineNumber()).build();
			auditRecordMapper.insert(record);
		}
	}

	/**
	 * 获得插入字符串
	 * @param object 对象
	 * @param field 属性
	 * @param audit 注解
	 */
	public String getShowValue(Object object, Field field, Audit audit) {
		// 空处理
		if (Objects.isNull(object)) {
			return null;
		}
		// 获取值
		Object fieldValue = reflectInvokeTool.getValueByGetter(field.getName(), object);
		// 空处理
		if (Objects.isNull(fieldValue)) {
			return "";
		}
		// 获取处理器
		Class<? extends ShowValueProcessor> showValueProcessorClass = audit.showValueProcessor();
		return (String) reflectInvokeTool.invokeMethod(showValueProcessorClass, "getShowString",
				beanFactory.getBean(showValueProcessorClass), fieldValue);
	}

	@AllArgsConstructor
	@Getter
	public enum AuditType {

		INSERT("新增"), UPDATE("更新"), DELETE("删除");

		private final String name;

	}

}