package com.jiang.ddd.aop.audit;

import org.springframework.stereotype.Component;

@Component
public class DefaultShowValueProcessor implements ShowValueProcessor {

	@Override
	public String getShowString(Object fieldValue) {
		return fieldValue.toString();
	}

}
