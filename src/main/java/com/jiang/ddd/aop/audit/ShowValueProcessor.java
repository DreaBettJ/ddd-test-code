package com.jiang.ddd.aop.audit;

/**
 * 展示处理接口
 *
 * @author jiang.li
 * @since 2021/11/8
 */
public interface ShowValueProcessor {

	/**
	 * 获取对应的展示字符串
	 * @param fieldValue 属性值
	 * @return 字符串
	 */
	String getShowString(Object fieldValue);

}
