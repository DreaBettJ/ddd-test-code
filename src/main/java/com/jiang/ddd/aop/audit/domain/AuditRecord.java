package com.jiang.ddd.aop.audit.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.jiang.ddd.business.base.BasicEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 数据库审计记录
 * </p>
 *
 * @author jiang.li
 * @since 2021-11-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("audit_record")
@Builder
public class AuditRecord extends BasicEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 审计信息
	 */
	private String auditMessage;

	/**
	 * 表名
	 */
	private String tableName;

	/**
	 * 记录表主键
	 */
	private Long tableId;

	/**
	 * 操作类型
	 */
	private String operateType;

	/**
	 * 操作方法信息
	 */
	private String operateMethod;

	/**
	 * 调用方法行数
	 */
	private Integer operateMethodLineNumber;

	/**
	 * 链路id
	 */
	private String traceId;

}
