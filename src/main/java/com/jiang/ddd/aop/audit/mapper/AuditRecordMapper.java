package com.jiang.ddd.aop.audit.mapper;

import com.jiang.ddd.aop.audit.domain.AuditRecord;
import com.jiang.ddd.business.base.BasicMapper;

/**
 * <p>
 * 数据库审计记录 Mapper 接口
 * </p>
 *
 * @author jiang.li
 * @since 2021-11-15
 */
public interface AuditRecordMapper extends BasicMapper<AuditRecord> {

}
