package com.jiang.ddd.aop.extend;

import org.springframework.aop.framework.AopContext;

/**
 * 当前代理
 *
 * @param <S> 代理类型
 */
public interface SelfInterface<S> {

	/**
	 * 获取当前代理对象
	 */
	default S self() {
		if (this.getClass().isInstance(AopContext.currentProxy())) {
			return (S) this.getClass().cast(AopContext.currentProxy());
		}
		else {
			return (S) this;
		}
	}

}
