package com.jiang.ddd.aop.interceptor;

import com.jiang.ddd.context.BasicContext;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 清除ThreadLocal上下文，拦截器
 *
 * @author jiang.li
 * @since 2021/10/27
 */
@Component
public class ContextClearInterceptor implements HandlerInterceptor {

	@Resource
	List<BasicContext> contextList;

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) {
		contextList.forEach(BasicContext::clear);
	}

}
