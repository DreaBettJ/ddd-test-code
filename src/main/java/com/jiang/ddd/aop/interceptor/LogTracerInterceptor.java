package com.jiang.ddd.aop.interceptor;

import com.jiang.ddd.tool.TranceIdGenerator;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.jiang.ddd.constant.AppConstants.AOP.TRANCE_ID_KEY;

/**
 * 日志追踪拦截器
 *
 * @author jinag.li
 */
@Slf4j
@Component
public class LogTracerInterceptor implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
		String traceId = TranceIdGenerator.generate();
		MDC.put(TRANCE_ID_KEY, traceId);
		response.addHeader(TRANCE_ID_KEY, traceId);
		return true;
	}

}
