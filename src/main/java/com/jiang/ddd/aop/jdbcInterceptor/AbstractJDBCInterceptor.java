package com.jiang.ddd.aop.jdbcInterceptor;

import com.jiang.ddd.DddApplication;
import com.jiang.ddd.context.SpringContextHolder;
import com.jiang.ddd.tool.ReflectInfoTool;
import com.jiang.ddd.tool.StackTool;
import com.jiang.ddd.tool.StringTool;
import com.mysql.cj.MysqlConnection;
import com.mysql.cj.Query;
import com.mysql.cj.conf.DefaultPropertySet;
import com.mysql.cj.interceptors.QueryInterceptor;
import com.mysql.cj.log.Log;
import com.mysql.cj.protocol.Resultset;
import com.mysql.cj.protocol.ServerSession;
import com.mysql.cj.result.Field;
import com.mysql.cj.result.ValueFactory;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.*;
import java.util.function.Supplier;

/**
 * 拦截器的抽象，提取共性
 *
 * @author jiang.li
 * @since 2021/12/5
 */
public abstract class AbstractJDBCInterceptor implements QueryInterceptor {

	/**
	 * JDBC 值处理器,用于读取 ResultRow中的值
	 */
	protected final Map<String, ValueFactory> processMap = new HashMap<>();

	@Resource
	protected SpringContextHolder springContextHolder;

	@Resource
	ReflectInfoTool reflectInfoTool;

	/**
	 * 记录注册了哪些拦截器，用于忽略来自这些拦截器的sql
	 */
	private static String[] interceptors;

	@PostConstruct
	private void springInit() {
		// 初始化JDBC值处理器
		List<Class<?>> factoryClasses = reflectInfoTool.getClassInPackageAndExtendClass(ValueFactory.class,
				ValueFactory.class.getPackage().getName());
		factoryClasses.forEach(clazz -> {
			Object o = reflectInfoTool.haveArgInstance(clazz, new DefaultPropertySet());
			if (o instanceof ValueFactory) {
				ValueFactory valueFactory = (ValueFactory) o;
				processMap.put(valueFactory.getTargetTypeName(), valueFactory);
			}
		});
	}

	@Override
	public QueryInterceptor init(MysqlConnection conn, Properties props, Log log) {
		String interceptorsStr = String.valueOf(props.get("queryInterceptors"));
		interceptors = interceptorsStr.split(",");
		return springContextHolder.applicationContext.getBean(this.getClass());
	}

	@Override
	public boolean executeTopLevelOnly() {
		return true;
	}

	@Override
	public void destroy() {
	}

	@Override
	public <T extends Resultset> T preProcess(Supplier<String> sql, Query interceptedQuery) {
		if (isIgnore()) {
			return null;
		}
		return beforeProcess(sql, interceptedQuery);
	}

	public abstract <T extends Resultset> T beforeProcess(Supplier<String> sql, Query interceptedQuery);

	@Override
	public <T extends Resultset> T postProcess(Supplier<String> sql, Query interceptedQuery, T originalResultSet,
			ServerSession serverSession) {
		if (isIgnore()) {
			return originalResultSet;
		}
		return afterProcess(sql, interceptedQuery, originalResultSet, serverSession);
	}

	public abstract <T extends Resultset> T afterProcess(Supplier<String> sql, Query interceptedQuery,
			T originalResultSet, ServerSession serverSession);

	/**
	 * 判定是否跳过拦截
	 */
	private boolean isIgnore() {
		// 通过堆栈判定是否是来自拦截器的sql，拦截器sql默认不拦截
		StackTraceElement lastStackInPackages = StackTool.getLastStackInPackages(new Throwable(), interceptors);
		return Objects.nonNull(lastStackInPackages) || !DddApplication.isInited;
	}

	/**
	 * 根据field获取对应的valueFactory
	 */
	protected ValueFactory getValueFactory(Field field) {
		Class<? extends Object> javaClass = reflectInfoTool.getFieldByName(field.getMysqlType(), "javaClass",
				Class.class);
		return processMap.get(javaClass.getName());
	}

	/**
	 * 获取sql方法
	 */
	protected String getMethod(String sqlStr) {
		return Objects.requireNonNull(StringTool.getWord(sqlStr, 1)).toLowerCase();
	}

}
