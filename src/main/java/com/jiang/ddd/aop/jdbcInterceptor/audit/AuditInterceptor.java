package com.jiang.ddd.aop.jdbcInterceptor.audit;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.jiang.ddd.aop.audit.AuditUtils;
import com.jiang.ddd.aop.jdbcInterceptor.AbstractJDBCInterceptor;
import com.jiang.ddd.business.base.BasicEntity;
import com.jiang.ddd.business.base.BasicMapper;
import com.jiang.ddd.constant.AppConstants;
import com.jiang.ddd.tool.ReflectInfoTool;
import com.jiang.ddd.tool.ReflectInvokeTool;
import com.jiang.ddd.tool.SqlTool;
import com.jiang.ddd.tool.StringTool;
import com.mysql.cj.Query;
import com.mysql.cj.protocol.Resultset;
import com.mysql.cj.protocol.ServerSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.function.Supplier;

/**
 * 记录entity的实体变化
 *
 * @author jiang.li
 * @since 2021/11/16
 */
@Slf4j
@Component
public class AuditInterceptor extends AbstractJDBCInterceptor {

	/**
	 * entity类 field 信息
	 */
	private Map<String, Map<String, Field>> classFieldNameInfoMap;

	/**
	 * 类信息
	 */
	private Map<String, Class> classInfo;

	/**
	 * 实体类 -> 对应mapper
	 */
	private Map<Class, BasicMapper> entityClassToMapper;

	@Value("${mybatis-plus.global-config.db-config.logic-delete-value}")
	private Integer deletedValue;

	@Resource
	ExecutorService logPool;

	@Resource
	private AuditUtils auditUtils;

	@Resource
	ReflectInfoTool reflectInfoTool;

	@Resource
	ReflectInvokeTool reflectInvokeTool;

	/**
	 * 记录之前栈
	 */
	private final ThreadLocal<Throwable> throwable = new ThreadLocal<>();

	@PostConstruct
	public void springInit() {
		List<Class<?>> entityClasses = reflectInfoTool.getClassInPackageAndExtendClass(BasicEntity.class,
				AppConstants.Package.ENTITY);
		List<Class<?>> mapperClasses = reflectInfoTool.getClassInPackageAndExtendClass(BasicMapper.class,
				AppConstants.Package.MAPPER);
		classFieldNameInfoMap = reflectInfoTool.getClassFieldNameInfoMap(entityClasses);
		classInfo = reflectInfoTool.getClassInfo(entityClasses);
		// 从Spring上下文获取对应的mapper
		entityClassToMapper = new HashMap<>();
		mapperClasses.forEach(mapperClass -> {
			Class<?> entityClass = reflectInfoTool.getInterfaceActualType(mapperClass, 0, 0);
			BasicMapper mapper = (BasicMapper) springContextHolder.applicationContext.getBean(mapperClass);
			entityClassToMapper.put(entityClass, mapper);
		});
	}

	@Override
	public <T extends Resultset> T beforeProcess(Supplier<String> sql, Query interceptedQuery) {
		Throwable throwable = new Throwable();
		String sqlStr = sql.get();
		String tableName = SqlTool.getTableName(sqlStr);
		// 获取到表名
		if (Objects.nonNull(tableName)) {
			String entityClassName = StringTool.toClassNameForm(tableName);
			Class entityClass = classInfo.get(entityClassName);
			// 属于entity类
			if (Objects.nonNull(entityClass)) {
				String method = super.getMethod(sqlStr);
				BasicMapper mapper = entityClassToMapper.get(entityClass);
				switch (method) {
				case "update": {
					// 同步搜索，防止sql已经结束了，还没有查到
					List<BasicEntity> beforeEntities = mapper
							.selectList(Wrappers.query().apply(StringTool.getAfter(sqlStr.toLowerCase(), "where")));
					// 异步记录
					logPool.execute(() -> {
						this.throwable.set(throwable);
						BasicEntity targetEntity = getTargetEntity(sqlStr, entityClassName, entityClass);
						beforeEntities.forEach(beforeEntity -> {
							if (Objects.equals(deletedValue, targetEntity.getDeleted())) {
								auditUtils.processLog(AuditUtils.AuditType.DELETE, beforeEntity, null,
										this.throwable.get());
							}
							else {
								auditUtils.processLog(AuditUtils.AuditType.UPDATE, beforeEntity, targetEntity,
										this.throwable.get());
							}
						});
					});
					break;
				}
				case "delete": {
					// 同步搜索，防止sql已经结束了，还没有查到
					List<BasicEntity> beforeEntities = mapper
							.selectList(Wrappers.query().apply(StringTool.getAfter(sqlStr.toLowerCase(), "where")));
					logPool.execute(() -> {
						this.throwable.set(throwable);
						beforeEntities.forEach(beforeEntity -> auditUtils.processLog(AuditUtils.AuditType.DELETE,
								beforeEntity, null, this.throwable.get()));
					});
					break;
				}
				}
			}
		}
		return null;
	}

	@Override
	public <T extends Resultset> T afterProcess(Supplier<String> sql, Query interceptedQuery, T originalResultSet,
			ServerSession serverSession) {
		Throwable throwable = new Throwable();
		logPool.execute(() -> {
			this.throwable.set(throwable);
			auditInsert(sql);
		});
		return originalResultSet;
	}

	/**
	 * 新增审计
	 */
	private void auditInsert(Supplier<String> sql) {
		String sqlStr = sql.get();
		String tableName = SqlTool.getTableName(sqlStr);
		// 获取到表名
		if (Objects.nonNull(tableName)) {
			String entityClassName = StringTool.toClassNameForm(tableName);
			Class entityClass = classInfo.get(entityClassName);
			BasicMapper mapper = entityClassToMapper.get(entityClass);
			// 属于entity类
			if (Objects.nonNull(entityClass)) {
				String method = getMethod(sqlStr);
				if ("insert".equals(method)) {
					BasicEntity entity = getTargetEntity(sqlStr, entityClassName, entityClass);
					// 如果没有id，补充id
					if (Objects.isNull(entity.getId())) {
						entity.setId(mapper.getNewAutoId());
					}
					auditUtils.processLog(AuditUtils.AuditType.INSERT, null, entity, throwable.get());
				}
			}
		}
	}

	/**
	 * 获取目标entity
	 */
	private BasicEntity getTargetEntity(String sqlStr, String entityClassName, Class entityClass) {
		Map<String, String> fieldMap = SqlTool.getFieldMap(sqlStr);
		Map<String, Field> stringFieldMap = classFieldNameInfoMap.get(entityClassName);
		// 生成entity
		return (BasicEntity) reflectInvokeTool.createObjectByFields(fieldMap, stringFieldMap, entityClass);
	}

}