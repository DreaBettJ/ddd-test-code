package com.jiang.ddd.aop.jdbcInterceptor.field;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.LocalDateTimeUtil;
import com.jiang.ddd.aop.jdbcInterceptor.AbstractJDBCInterceptor;
import com.jiang.ddd.tool.ReflectInfoTool;
import com.mysql.cj.Query;
import com.mysql.cj.jdbc.result.ResultSetImpl;
import com.mysql.cj.protocol.ColumnDefinition;
import com.mysql.cj.protocol.Resultset;
import com.mysql.cj.protocol.ResultsetRows;
import com.mysql.cj.protocol.ServerSession;
import com.mysql.cj.result.AbstractDateTimeValueFactory;
import com.mysql.cj.result.Field;
import com.mysql.cj.result.Row;
import com.mysql.cj.result.ValueFactory;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.function.Supplier;

/**
 * 脱敏拦截器
 *
 * @author jiang.li
 * @since 2021/12/4
 */
@Component
public class JDBCFieldInterceptor extends AbstractJDBCInterceptor {

	@Resource
	private List<JDBCFileProcessor> jdbcProcessors;

	@Resource
	ReflectInfoTool reflectInfoTool;

	@Override
	@SneakyThrows
	public <T extends Resultset> T afterProcess(Supplier<String> sql, Query interceptedQuery, T originalResultSet,
			ServerSession serverSession) {
		String method = super.getMethod(sql.get());
		if (originalResultSet instanceof ResultSetImpl && "select".equalsIgnoreCase(method)) {
			ResultSetImpl resultSet = (ResultSetImpl) originalResultSet;
			ColumnDefinition metadata = resultSet.getMetadata();
			// 处理每一行
			ResultsetRows rows = resultSet.getRows();
			// ps ：请不要使用迭代器遍历，因为 mybatis 使用了之后用的迭代器遍历，next会改变索引值
			for (int rowIndex = 0; rowIndex < rows.size(); rowIndex++) {
				Row row = rows.get(rowIndex);
				Field[] fields = metadata.getFields();
				for (int fieldIndex = 0; fieldIndex < fields.length; fieldIndex++) {
					Field field = fields[fieldIndex];
					ValueFactory valueFactory = super.getValueFactory(field);
					Object value = row.getValue(fieldIndex, valueFactory);

					// 遍历属性处理器
					for (JDBCFileProcessor jdbcProcessor : jdbcProcessors) {
						Class<?> parameterClass = reflectInfoTool.getInterfaceActualType(jdbcProcessor.getClass(), 0,
								0);
						if (parameterClass.isInstance(value)) {
							Object processed = jdbcProcessor.process(value, field.getName());
							String result = processed.toString();
							// 时间需要固定格式才能被之后解析成功
							if (valueFactory instanceof AbstractDateTimeValueFactory) {
								String format = "yyyy-MM-dd HH:mm:ss";
								try {
									LocalDateTime dateTime = LocalDateTimeUtil.parse(result);
									result = dateTime.format(DateTimeFormatter.ofPattern(format));
								}
								catch (Exception exception) {
									DateTime dateTime = DateUtil.parse(result);
									result = dateTime.toString();
								}
							}
							row.setBytes(fieldIndex, result.getBytes(field.getEncoding()));
						}
					}
				}
			}
		}
		return originalResultSet;
	}

	@Override
	public <T extends Resultset> T beforeProcess(Supplier<String> sql, Query interceptedQuery) {
		return null;
	}

}
