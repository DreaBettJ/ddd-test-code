package com.jiang.ddd.aop.jdbcInterceptor.field;

/**
 * JDBC拦截器处理参数
 *
 * @param <P> 处理的类型
 */
public interface JDBCFileProcessor<P> {

	/**
	 * jdbc属性拦截处理
	 * @param p 入参，就是获得到的entity的属性
	 * @param name 属性名称
	 * @return 处理后的结果
	 */
	P process(P p, String name);

}
