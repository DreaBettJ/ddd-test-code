package com.jiang.ddd.aop.jdbcInterceptor.field.processer;

import com.jiang.ddd.aop.jdbcInterceptor.field.JDBCFileProcessor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class BigDecimalProcessor implements JDBCFileProcessor<Double> {

	@Override
	public Double process(Double aDouble, String name) {
		return BigDecimal.valueOf(aDouble).movePointLeft(2).doubleValue();
	}

}
