package com.jiang.ddd.aop.jdbcInterceptor.field.processer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 用于标记该请求身份证是否脱敏
 *
 * @author jiang.li
 * @since 2021/12/4
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DesensitizeForIdCard {

	/**
	 * 前半部分默认值
	 */
	int defaultStartLength = 3;

	/**
	 * 后半部分默认值
	 */
	int defaultEndLength = 3;

	/**
	 * 是否禁用
	 */
	boolean disable() default false;

	/**
	 * 前半部分不脱敏长度
	 */
	int startLength() default defaultStartLength;

	/**
	 * 后半部分不脱敏长度
	 */
	int endLength() default defaultEndLength;

}