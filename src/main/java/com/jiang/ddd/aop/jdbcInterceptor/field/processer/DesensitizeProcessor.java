package com.jiang.ddd.aop.jdbcInterceptor.field.processer;

import com.jiang.ddd.aop.jdbcInterceptor.field.JDBCFileProcessor;
import com.jiang.ddd.context.ControllerAnnoMarkContext;
import com.jiang.ddd.tool.RegexTool;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Objects;

@Component
public class DesensitizeProcessor implements JDBCFileProcessor<String> {

	@Resource
	private ControllerAnnoMarkContext annoMarkContext;

	@Override
	public String process(String str, String name) {
		// 身份证脱敏
		DesensitizeForIdCard desensitizeForIdCard = annoMarkContext.get(DesensitizeForIdCard.class);
		boolean idCardMarkDisable = Objects.nonNull(desensitizeForIdCard) && desensitizeForIdCard.disable();
		if (RegexTool.contain(RegexTool.REGEX_ID_CARD, str) && !idCardMarkDisable) {
			String idCard;
			if (Objects.nonNull(desensitizeForIdCard)) {
				idCard = RegexTool.desensitize(RegexTool.REGEX_ID_CARD, str, desensitizeForIdCard.startLength(),
						desensitizeForIdCard.endLength());
			}
			else {
				idCard = RegexTool.desensitize(RegexTool.REGEX_ID_CARD, str, DesensitizeForIdCard.defaultStartLength,
						DesensitizeForIdCard.defaultEndLength);
			}
			return idCard;
		}

		// 手机号脱敏
		DesensitizeForPhone desensitizePhone = annoMarkContext.get(DesensitizeForPhone.class);
		boolean phoneMarkDisable = Objects.nonNull(desensitizePhone) && desensitizePhone.disable();
		if (RegexTool.contain(RegexTool.REGEX_PHONE, str) && !phoneMarkDisable) {
			String phone;
			if (Objects.nonNull(desensitizePhone)) {
				phone = RegexTool.desensitize(RegexTool.REGEX_PHONE, str, desensitizeForIdCard.startLength(),
						desensitizeForIdCard.endLength());
			}
			else {
				phone = RegexTool.desensitize(RegexTool.REGEX_PHONE, str, DesensitizeForPhone.defaultStartLength,
						DesensitizeForPhone.defaultEndLength);
			}
			return phone;
		}
		return str;
	}

}
