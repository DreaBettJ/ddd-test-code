package com.jiang.ddd.aop.jdbcInterceptor.field.processer;

import com.jiang.ddd.aop.jdbcInterceptor.field.JDBCFileProcessor;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class LongProcess implements JDBCFileProcessor<Integer> {

	@Override
	public Integer process(Integer integer, String name) {
		if (Objects.equals(0, integer)) {
			return 10000;
		}
		return integer;
	}

}
