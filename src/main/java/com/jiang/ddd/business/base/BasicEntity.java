package com.jiang.ddd.business.base;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jiang.ddd.aop.audit.Audit;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 抽象entity
 *
 * @author jiang.li
 * @since 2021/8/25
 */
@Data
@NoArgsConstructor
public abstract class BasicEntity {

	@TableId(value = "id", type = IdType.ASSIGN_ID)
	@Audit(alias = "主键", isFixed = true)
	protected Long id;

	/**
	 * 删除标记
	 */
	@Audit(ignore = true)
	protected Integer deleted;

	/**
	 * 创建时间
	 */
	@TableField(value = "create_time", fill = FieldFill.INSERT)
	@Audit(alias = "创建时间")
	protected LocalDateTime createTime;

	/**
	 * 修改时间
	 */
	@TableField(value = "update_time", fill = FieldFill.UPDATE)
	@Audit(alias = "修改时间")
	protected LocalDateTime updateTime;

}
