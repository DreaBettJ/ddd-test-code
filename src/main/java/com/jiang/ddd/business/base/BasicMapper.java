package com.jiang.ddd.business.base;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 抽象mapper
 *
 * @author jiang.li
 * @since 2021/8/25
 */
public interface BasicMapper<T extends BasicEntity> extends BaseMapper<T> {

	/**
	 * 获取自动生成主键的值
	 * @return 新主键
	 */
	Long getNewAutoId();

}
