package com.jiang.ddd.business.base;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

/**
 * 抽象model
 *
 * @author jiang.li
 * @since 2021/8/25
 */
@Data
@RequiredArgsConstructor
@NoArgsConstructor
public abstract class BasicModel<E extends BasicEntity, M extends BasicMapper<E>> {

	/**
	 * 主键
	 */
	@NonNull
	protected Long id;

	/**
	 * 删除标记
	 */
	protected Integer deleted;

	/**
	 * 创建时间
	 */
	protected LocalDateTime createTime;

	/**
	 * 修改时间
	 */
	protected LocalDateTime updateTime;

	@Autowired
	protected M baseMapper;

	/**
	 * 初始化
	 */
	public void init() {
		M baseMapper = this.getBaseMapper();
		E entity = baseMapper.selectById(this.id);
		BeanUtils.copyProperties(entity, this);
	}

}