package com.jiang.ddd.business.base;

import com.baomidou.mybatisplus.extension.service.IService;

public interface BasicService<T extends BasicEntity> extends IService<T> {

}
