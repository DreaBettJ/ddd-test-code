package com.jiang.ddd.business.base;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jiang.ddd.aop.extend.SelfInterface;

/**
 * 抽象Service
 *
 * @author jiang.li
 * @since 2021/8/25
 */
public abstract class BasicServiceImpl<M extends BasicMapper<T>, T extends BasicEntity, S extends BasicServiceImpl<M, T, ?>>
		extends ServiceImpl<M, T> implements BasicService<T>, SelfInterface<S> {

}
