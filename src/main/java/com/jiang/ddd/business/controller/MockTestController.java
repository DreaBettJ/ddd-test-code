package com.jiang.ddd.business.controller;

import com.jiang.ddd.business.base.BasicController;
import com.jiang.ddd.business.domain.MockTest;
import com.jiang.ddd.business.service.iservice.IMockTestService;
import com.jiang.ddd.common.dto.CommonResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * test mock 前端控制器
 *
 * @author jiang.li
 * @since 2021-09-29
 */
@RestController
@RequestMapping("/mock-test")
@Validated
public class MockTestController extends BasicController {
	@Resource
	IMockTestService iMockTestService;

	/**
	 * 测试请求1
	 * @param id 主键
	 * @return 结果
	 */
	@GetMapping("/test")
	public CommonResult<MockTest> test(Long id) {
		return CommonResult.success(iMockTestService.test(id));
	}

}

