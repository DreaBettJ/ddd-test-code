package com.jiang.ddd.business.domain;

import com.jiang.ddd.business.base.BasicEntity;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * test mock
 * </p>
 *
 * @author jiang.li
 * @since 2021-09-29
 */
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@RequiredArgsConstructor
public class MockTest extends BasicEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * test value
	 */
	@NonNull
	private String value;

	private String value2;

	private BigDecimal value3;

}
