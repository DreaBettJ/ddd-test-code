package com.jiang.ddd.business.mapper;

import com.jiang.ddd.business.base.BasicMapper;
import com.jiang.ddd.business.domain.MockTest;

/**
 * <p>
 * test mock Mapper 接口
 * </p>
 *
 * @author jiang.li
 * @since 2021-09-29
 */
public interface MockTestMapper extends BasicMapper<MockTest> {

	MockTest testSelect();

}
