package com.jiang.ddd.business.model;

import com.jiang.ddd.aop.annotation.ServiceMethod;
import com.jiang.ddd.business.base.BasicModel;
import com.jiang.ddd.business.domain.MockTest;
import com.jiang.ddd.business.mapper.MockTestMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

@EqualsAndHashCode(callSuper = true)
@Data
@Slf4j
public class MockTestModel extends BasicModel<MockTest, MockTestMapper> {

	/**
	 * test value
	 */
	private Integer value;

	public MockTestModel(Long id) {
		super(id);
	}

	/**
	 * 测试方法
	 */
	@ServiceMethod
	public String test(Long id) {
		log.info("AOP测试：{}", id);
		return String.valueOf(this.value);
	}

}
