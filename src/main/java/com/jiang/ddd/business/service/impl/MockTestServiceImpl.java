package com.jiang.ddd.business.service.impl;

import com.jiang.ddd.business.base.BasicServiceImpl;
import com.jiang.ddd.business.domain.MockTest;
import com.jiang.ddd.business.mapper.MockTestMapper;
import com.jiang.ddd.business.service.iservice.IMockTestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * <p>
 * test mock 服务实现类
 * </p>
 *
 * @author jiang.li
 * @since 2021-09-29
 */
@Service
@Slf4j
public class MockTestServiceImpl extends BasicServiceImpl<MockTestMapper, MockTest, MockTestServiceImpl>
		implements IMockTestService {

	@Override
	public MockTest test(Long id) {
		MockTest mockTest = new MockTest("12312312321");
		mockTest.setValue2("21312312");
		baseMapper.insert(mockTest);

        String test = self().test123(id);

		return self().getById(id);
	}

	public String test123(Long id) {
		return id.toString();
	}

	@Override
	public void test2(Long id) {
		System.out.println(id);
	}

}
