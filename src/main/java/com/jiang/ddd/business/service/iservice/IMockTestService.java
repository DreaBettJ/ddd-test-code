package com.jiang.ddd.business.service.iservice;

import com.jiang.ddd.business.base.BasicService;
import com.jiang.ddd.business.domain.MockTest;

/**
 * <p>
 * test mock 服务类
 * </p>
 *
 * @author jiang.li
 * @since 2021-09-29
 */
public interface IMockTestService extends BasicService<MockTest> {

	/**
	 * 测试方法
	 * @param id id
	 * @return 方法测试
	 */
	MockTest test(Long id);

	void test2(Long id);

}
