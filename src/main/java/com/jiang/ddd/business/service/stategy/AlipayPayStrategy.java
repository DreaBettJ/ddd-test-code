package com.jiang.ddd.business.service.stategy;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class AlipayPayStrategy implements PayStrategy {

	@Override
	public void pay(BigDecimal payAmount) {
		System.out.println("ali pay:" + payAmount.toPlainString() + " yuan");
	}

}
