package com.jiang.ddd.business.service.stategy;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PayMethodEnum {

	wechat(WechatPayStrategy.class), ali(AlipayPayStrategy.class);

	private Class<? extends PayStrategy> strategyClass;

}
