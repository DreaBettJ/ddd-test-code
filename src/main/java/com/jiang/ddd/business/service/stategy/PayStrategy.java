package com.jiang.ddd.business.service.stategy;

import java.math.BigDecimal;

public interface PayStrategy {

	void pay(BigDecimal payAmount);

}
