package com.jiang.ddd.chain;

import lombok.Data;
import lombok.NonNull;

import java.util.List;

/**
 * 链条接收
 *
 * @author jiang.li
 * @since 2021/11/26
 */
@Data
public class ChainInput {

	/**
	 * 客户端
	 */
	@NonNull
	private List<ClientEnum> clients;

	@NonNull
	private String title;

	@NonNull
	private String content;

}
