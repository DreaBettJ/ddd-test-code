package com.jiang.ddd.chain;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;

import java.util.List;

public interface ChainNode {

	/**
	 * 提醒抽象
	 */
	default void remind(ChainInput chainInput) {
		List<ClientEnum> clients = chainInput.getClients();
		if (CollectionUtils.isNotEmpty(clients) && clients.contains(getClient())) {
			remindDetail(chainInput);
		}
	};

	void remindDetail(ChainInput chainInput);

	ClientEnum getClient();

}
