package com.jiang.ddd.chain;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.core.ResolvableType;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class ClientChainContainer {

	@Resource
	private List<ChainNode> chainNodeList;

	public void processRemind(ChainInput chainInput) {
		for (ChainNode chainNode : chainNodeList) {
			chainNode.remind(chainInput);
		}
	}

	public void setChainNodeList(List<ChainNode> chainNodeList) {
		this.chainNodeList = chainNodeList;
	}

}
