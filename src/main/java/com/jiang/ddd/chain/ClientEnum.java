package com.jiang.ddd.chain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ClientEnum {

	DING_DING, WEB, EMAIL

}
