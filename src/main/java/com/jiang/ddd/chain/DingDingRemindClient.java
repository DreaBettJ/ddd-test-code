package com.jiang.ddd.chain;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class DingDingRemindClient implements ChainNode {

	@Override
	public void remindDetail(ChainInput chainInput) {
		System.out.printf(String.format("钉钉提醒,标题%s,内容%s", chainInput.getTitle(), chainInput.getContent()));
	}

	@Override
	public ClientEnum getClient() {
		return ClientEnum.DING_DING;
	}

}
