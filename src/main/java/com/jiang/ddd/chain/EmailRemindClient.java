package com.jiang.ddd.chain;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(value = 3)
public class EmailRemindClient implements ChainNode {

	@Override
	public void remindDetail(ChainInput chainInput) {
		System.out.println("邮件提醒");
	}

	@Override
	public ClientEnum getClient() {
		return ClientEnum.EMAIL;
	}

}
