package com.jiang.ddd.chain;

import org.springframework.stereotype.Component;

@Component
public class WebRemindClient implements ChainNode {

	@Override
	public void remindDetail(ChainInput chainInput) {
		System.out.printf("web提醒");
	}

	@Override
	public ClientEnum getClient() {
		return ClientEnum.WEB;
	}

}
