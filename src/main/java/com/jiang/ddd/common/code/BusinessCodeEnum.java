package com.jiang.ddd.common.code;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum BusinessCodeEnum {

	// @formatter:off
    USER_FORBID("user_forbid","该用户禁止访问");
    // @formatter:on
	/**
	 * 代码
	 */
	private final String code;

	/**
	 * 描述
	 */
	private final String describe;

}
