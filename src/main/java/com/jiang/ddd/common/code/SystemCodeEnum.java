package com.jiang.ddd.common.code;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum SystemCodeEnum {

	// @formatter:off
	SYS_ERROR("sys_error","请求成功"),
	SERVLET_ERROR("servlet_error","服务器异常"),
	MISSING_REQUEST_PARAM_ERROR("missing_request_param_error","请求异常"),
	INVALID_REQUEST_PARAM_ERROR("invalid_request_param_error","请求参数确实"),
	METHOD_NOT_SUPPORT("method_not_support","请求参数不合法"),
	BAD_REQUEST_BODY("bad_request_body","方法不支持");
	// @formatter:on
	/**
	 * 代码
	 */
	private final String code;

	/**
	 * 描述
	 */
	private final String describe;

}