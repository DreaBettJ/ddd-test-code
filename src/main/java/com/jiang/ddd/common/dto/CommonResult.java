package com.jiang.ddd.common.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.jiang.ddd.tool.I18nTool;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.util.Assert;

import java.io.Serializable;

/**
 * 通用返回结果
 *
 * @author jiang.li
 * @since 2021/12/12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class CommonResult<T> implements Serializable {

	public static final String CODE_SUCCESS = "success";

	/**
	 * 错误码
	 */
	private String code;

	/**
	 * 错误提示
	 */
	private String message;

	/**
	 * 返回数据
	 */
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private T data;

	/**
	 * 将传入的 result 对象，转换成另外一个泛型结果的对象
	 *
	 * 因为 A 方法返回的 CommonResult 对象，不满足调用其的 B 方法的返回，所以需要进行转换。
	 * @param result 传入的 result 对象
	 * @param <T> 返回的泛型
	 * @return 新的 CommonResult 对象
	 */
	public static <T> CommonResult<T> error(CommonResult<?> result) {
		return error(result.getCode(), result.getMessage());
	}

	public static <T> CommonResult<T> error(String code) {
		return error(code, I18nTool.getMessage(code));
	}

	public static <T> CommonResult<T> error(String code, String message) {
		Assert.isTrue(!CODE_SUCCESS.equals(code), "code 必须是错误的！");
		CommonResult<T> result = new CommonResult<>();
		result.code = code;
		result.message = message;
		return result;
	}

	public static CommonResult<Void> success() {
		return success(null);
	}

	public static <T> CommonResult<T> success(T data) {
		CommonResult<T> result = new CommonResult<>();
		result.code = CODE_SUCCESS;
		result.message = I18nTool.getMessage(CODE_SUCCESS);
		result.data = data;
		return result;
	}

	public boolean isSuccess() { // 方便判断是否成功
		return CODE_SUCCESS.equals(code);
	}

}
