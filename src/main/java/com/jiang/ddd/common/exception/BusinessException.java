package com.jiang.ddd.common.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

/**
 * 接口请求异常
 *
 * @author DreaBettJ
 * @since 2020/12/24
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BusinessException extends RuntimeException {

	/**
	 * code 对应 错误
	 */
	@NonNull
	private String code;

}