package com.jiang.ddd.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Configuration
public class JacksonConfig {

	@Bean
	@Primary
	public ObjectMapper objectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
		// 设置java.util.Date时间类的序列化以及反序列化的格式
		objectMapper.setDateFormat(new SimpleDateFormat());

		// 初始化JavaTimeModule
		JavaTimeModule javaTimeModule = new JavaTimeModule();

		// 处理LocalDateTime
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_DATE_TIME;
		javaTimeModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(dateTimeFormatter));
		javaTimeModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(dateTimeFormatter));

		// 处理LocalDate
		DateTimeFormatter dateFormatter = DateTimeFormatter.ISO_DATE;
		javaTimeModule.addSerializer(LocalDate.class, new LocalDateSerializer(dateFormatter));
		javaTimeModule.addDeserializer(LocalDate.class, new LocalDateDeserializer(dateFormatter));

		// 处理LocalTime
		DateTimeFormatter timeFormatter = DateTimeFormatter.ISO_TIME;
		javaTimeModule.addSerializer(LocalTime.class, new LocalTimeSerializer(timeFormatter));
		javaTimeModule.addDeserializer(LocalTime.class, new LocalTimeDeserializer(timeFormatter));

		// 注册时间模块, 支持支持jsr310, 即新的时间类(java.time包下的时间类)
		objectMapper.registerModule(javaTimeModule);

		// 包含所有字段
		objectMapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);

		// 在序列化一个空对象时时不抛出异常
		objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

		// 忽略反序列化时在json字符串中存在, 但在java对象中不存在的属性
		objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		// 处理枚举返回name()
		objectMapper.configure(SerializationFeature.WRITE_ENUMS_USING_INDEX, false);
		// 处理枚举name
		objectMapper.configure(SerializationFeature.WRITE_ENUM_KEYS_USING_INDEX, false);

		return objectMapper;
	}

}
