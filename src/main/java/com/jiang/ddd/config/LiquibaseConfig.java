package com.jiang.ddd.config;

import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * Liquibase 配置
 *
 * @author jiang.li
 * @since 2021/9/21
 */
@Configuration
public class LiquibaseConfig {

	@Bean
	public SpringLiquibase liquibase(@Qualifier("dataSource") DataSource dataSource) {
		SpringLiquibase liquibase = new SpringLiquibase();
		liquibase.setDataSource(dataSource);
		// 指定changelog的位置，这里使用的一个master文件引用其他文件的方式
		liquibase.setChangeLog("classpath:liquibase/master.xml");
		liquibase.setShouldRun(true);
		return liquibase;
	}

}
