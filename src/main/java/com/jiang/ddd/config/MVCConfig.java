package com.jiang.ddd.config;

import com.jiang.ddd.aop.interceptor.ContextClearInterceptor;
import com.jiang.ddd.aop.interceptor.LogTracerInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

@Configuration
public class MVCConfig implements WebMvcConfigurer {

	@Resource
	LogTracerInterceptor logTracerInterceptor;

	@Resource
	ContextClearInterceptor contextClearInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(contextClearInterceptor).addPathPatterns("/**");
		registry.addInterceptor(logTracerInterceptor).addPathPatterns("/**");
	}

}
