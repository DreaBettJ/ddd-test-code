package com.jiang.ddd.config;

import com.jiang.ddd.business.service.stategy.PayStrategy;
import com.jiang.ddd.tool.AOPTool;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Configuration
public class StrategyConfig {

	@Bean
	public Map<Class, PayStrategy> payStrategyMap(List<PayStrategy> payStrategyList) {
		return payStrategyList.stream().collect(Collectors
				.toMap(payStrategy -> AOPTool.cglibGetNoProxyClass(payStrategy.getClass()), Function.identity()));
	}

}
