package com.jiang.ddd.config;

import com.jiang.ddd.constant.AppConstants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.core.task.support.ExecutorServiceAdapter;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 配置不同线程池，交给Spring管理
 *
 * @author jiang.li
 * @since 2021/10/27
 */
@Configuration
public class ThreadPoolConfig {

	@Bean("logPool")
	@Primary
	@Profile(AppConstants.Profile.DEV)
	public ExecutorService logPoolDev() {
		return new ExecutorServiceAdapter(Runnable::run);
	}

	@Bean("logPool")
	public ExecutorService logPool() {
		return Executors.newFixedThreadPool(4);
	}

}
