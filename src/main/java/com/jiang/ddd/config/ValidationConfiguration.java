package com.jiang.ddd.config;// ValidationConfiguration.java

import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
public class ValidationConfiguration {

	/**
	 * 参考 {@link ValidationAutoConfiguration#defaultValidator()} 方法，构建 Validator Bean
	 * @return Validator 对象
	 */
	@Bean
	public Validator validator(MessageSource messageSource) {
		LocalValidatorFactoryBean validator = ValidationAutoConfiguration.defaultValidator();
		validator.setValidationMessageSource(messageSource);
		return validator;
	}

}