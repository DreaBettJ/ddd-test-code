package com.jiang.ddd.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.YamlMapFactoryBean;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.core.io.support.PropertySourceFactory;

import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * yaml 资源读取工厂，配合 @PropertySource 使用
 *
 * @author jiang.li
 * @since 2021/11/28
 */
public class YAMLPropertySourceFactory implements PropertySourceFactory {

	@Override
	public PropertySource<?> createPropertySource(String name, EncodedResource encodedResource) {
		YamlMapFactoryBean factoryBean = new YamlMapFactoryBean();
		factoryBean.setResources(encodedResource.getResource());
		Properties properties = mapToFullProperties(factoryBean.getObject());
		// 返回。此时不能像默认工厂那样返回ResourcePropertySource对象 ，要返回他的父类PropertiesPropertySource对象。
		return name != null ? new PropertiesPropertySource(name, properties)
				: new PropertiesPropertySource(encodedResource.getResource().getFilename(), properties);
	}

	/**
	 * map转为属性，完整的
	 */
	public Properties mapToFullProperties(Map<String, Object> map) {
		Properties properties = new Properties();
		mapRecursion(map, properties, "");
		return properties;
	}

	/**
	 * mapToFullProperties 递归方法
	 */
	private void mapRecursion(Map<String, Object> map, Properties properties, String prefix) {
		map.forEach((key, value) -> {
			String nowKey = StringUtils.isEmpty(prefix) ? key : String.format("%s.%s", prefix, key);
			properties.put(nowKey, value);
			if (value instanceof Map) {
				mapRecursion((Map<String, Object>) value, properties, nowKey);
			}
			else if (value instanceof List) {
				List<?> objects = (List<?>) value;
				for (int i = 0; i < objects.size(); i++) {
					properties.put(String.format("%s[%d]", nowKey, i), objects.get(i));
				}
			}
		});
	}

}