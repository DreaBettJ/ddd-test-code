package com.jiang.ddd.config.mybatisPlus;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.jiang.ddd.tool.StringTool;
import lombok.SneakyThrows;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 查询最大id的方法
 */
@Component
public class GetNewAutoIdMethod extends AbstractMethod {

	@Value("${spring.datasource.url}")
	private String jdbcUrl;

	@Override
	public MappedStatement injectMappedStatement(Class<?> mapperClass, Class<?> modelClass, TableInfo tableInfo) {
		String sql = String.format(
				"SELECT AUTO_INCREMENT FROM information_schema.tables where TABLE_SCHEMA = '%s' and table_name='%s'",
				StringTool.betweenStringWithMinDistance(jdbcUrl, "/", "?", 1), tableInfo.getTableName());
		String methodName = "getNewAutoId";
		SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, modelClass);
		return this.addSelectMappedStatementForOther(mapperClass, methodName, sqlSource, Long.class);
	}

}
