package com.jiang.ddd.config.mybatisPlus;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.jiang.ddd.business.base.BasicEntity;
import com.jiang.ddd.tool.PropertyTool;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * MyBatis Plus 自动填充
 *
 * @author Lusifer
 * @since v1.0.0
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        // 判断是否有相关字段
        this.strictInsertFill(metaObject, PropertyTool.getProperty(BasicEntity::getCreateTime), LocalDateTime.class, LocalDateTime.now()); // 起始版本
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.strictUpdateFill(metaObject, PropertyTool.getProperty(BasicEntity::getCreateTime), LocalDateTime.class, LocalDateTime.now()); // 起始版本
    }
}
