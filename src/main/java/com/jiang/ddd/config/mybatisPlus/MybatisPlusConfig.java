package com.jiang.ddd.config.mybatisPlus;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import com.baomidou.mybatisplus.core.injector.ISqlInjector;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class MybatisPlusConfig {

	@Bean
	public ISqlInjector myCommonSql(List<AbstractMethod> methods) {
		return new DefaultSqlInjector() {
			@Override
			public List<AbstractMethod> getMethodList(Class<?> mapperClass) {
				List<AbstractMethod> methodList = super.getMethodList(mapperClass);
				methodList.addAll(methods);
				return methodList;
			}
		};
	}

}
