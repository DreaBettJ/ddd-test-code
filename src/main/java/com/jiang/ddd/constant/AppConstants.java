package com.jiang.ddd.constant;

import com.jiang.ddd.aop.aspect.StackAspect;

/**
 * 公用常量
 *
 * @author jiang.li
 * @since 2021/10/2
 */
public interface AppConstants {

	/**
	 * AOP常量
	 */
	class AOP {

		/**
		 * 服务方法需要添加的切面
		 */
		public static final Class<?>[] SERVICE_METHOD_ASPECTS = new Class[] { StackAspect.class };

		/**
		 * 链路id key
		 */
		public static final String TRANCE_ID_KEY = "traceId";

	}

	class Profile {

		/**
		 * dev
		 */
		public static final String DEV = "dev";

	}

	class Package {

		/**
		 * 根路径
		 */
		public static final String ROOT = "com.jiang.ddd";

		/**
		 * AOP
		 */
		public static final String AOP = "com.jiang.ddd.aop";

		/**
		 * 业务路径
		 */
		public static final String BUSINESS = "com.jiang.ddd.business";

		/**
		 * mapper路径
		 */
		public static final String MAPPER = "com.jiang.ddd.business.mapper";

		/**
		 * 实体路径
		 */
		public static final String ENTITY = "com.jiang.ddd.business.domain";

	}

}
