package com.jiang.ddd.context;

/**
 * 上下文抽象
 *
 * @author jiang.li
 * @since 2021/10/27
 * @param <T> 整个数据源
 */
public interface BasicContext<T> {

	/**
	 * 获取上下文内容
	 * @return T
	 */
	T get();

	/**
	 * 设置内容
	 * @param t 内容
	 */
	void set(T t);

	/**
	 * 清除内容
	 */
	void clear();

}
