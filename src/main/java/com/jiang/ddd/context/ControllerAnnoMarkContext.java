package com.jiang.ddd.context;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 存储controller标记的注解
 *
 * @author jiang.li
 * @since 2021/12/4
 */
@Component
public class ControllerAnnoMarkContext implements BasicContext<Map<Class<?>, Object>> {

	/**
	 * 存储 注解 -> 注解对象
	 */
	private final ThreadLocal<Map<Class<?>, Object>> map = ThreadLocal.withInitial(HashMap::new);

	@Override
	public Map<Class<?>, Object> get() {
		return map.get();
	}

	@Override
	public void set(Map<Class<?>, Object> classObjectMap) {
		map.set(classObjectMap);
	}

	@Override
	public void clear() {
		map.remove();
	}

	/**
	 * 根据clazz对象获取注解实例
	 */
	public <T> T get(Class<T> clazz) {
		return clazz.cast(map.get().get(clazz));
	}

	/**
	 * set 方法
	 */
	public void set(Class<?> clazz, Object annoObject) {
		map.get().put(clazz, annoObject);
	}

}
