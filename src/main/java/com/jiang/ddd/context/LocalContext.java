package com.jiang.ddd.context;

import org.springframework.context.i18n.LocaleContext;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

/**
 * 承接Spring的local上下文，用于线程间上下文转移
 *
 * @author jiang.li
 * @since 2021/12/12
 */
@Component
public class LocalContext implements BasicContext<LocaleContext> {

	@Override
	public LocaleContext get() {
		return LocaleContextHolder.getLocaleContext();
	}

	@Override
	public void set(LocaleContext localeContext) {
		LocaleContextHolder.setLocaleContext(localeContext);
	}

	@Override
	public void clear() {
		LocaleContextHolder.resetLocaleContext();
	}

}
