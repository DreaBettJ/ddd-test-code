package com.jiang.ddd.context;

import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class MDCContext implements BasicContext<Map<String, String>> {

	@Override
	public Map<String, String> get() {
		return MDC.getCopyOfContextMap();
	}

	@Override
	public void set(Map<String, String> stringStringMap) {
		MDC.setContextMap(stringStringMap);
	}

	@Override
	public void clear() {
		MDC.clear();
	}

}
