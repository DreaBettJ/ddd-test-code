package com.jiang.ddd.context;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TestContext implements BasicContext<List<Long>> {

	private final ThreadLocal<List<Long>> test = ThreadLocal.withInitial(ArrayList::new);

	@Override
	public List<Long> get() {
		return test.get();
	}

	@Override
	public void set(List<Long> longs) {
		test.set(longs);
	}

	@Override
	public void clear() {
		test.remove();
	}

}
