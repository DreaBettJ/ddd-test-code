package com.jiang.ddd.runner;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.net.InetAddress;

/**
 * 启动后地址暴露
 *
 * @author jiang.li
 * @version v1.0.0
 * @since 2021/7/8
 */
@Component
@Slf4j
public class IpLogRunner implements CommandLineRunner {

	@Value("${server.port:8080}")
	private String port;

	@Override
	public void run(String... args) throws Exception {
		log.info(
				"Access URLs:\n----------------------------------------------------------\n\t"
						+ "Local: \t\thttp://127.0.0.1:{}\n\t"
						+ "External: \thttp://{}:{}\n----------------------------------------------------------",
				port, InetAddress.getLocalHost().getHostAddress(), port);
	}

}