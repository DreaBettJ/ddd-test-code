package com.jiang.ddd.tool;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.core.io.InputStreamSource;
import org.springframework.lang.NonNull;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * AOP工具类
 *
 * @author jiang.li
 * @since 2021/8/25
 */
@Slf4j
public class AOPTool {

	/**
	 * 替换请求参数
	 * @param point jp 切入点
	 * @param tClass 目标 class 用于定位
	 * @param index 目标 index 用于定位
	 * @param replaceTarget 用于 替换的参数
	 */
	public static <T> void replaceRequestParamByClazz(ProceedingJoinPoint point, Class<T> tClass, int index,
			T replaceTarget) {
		Object[] args = point.getArgs();
		int matchIndex = 0;
		for (int i = 0; i < args.length; i++) {
			Object arg = args[i];
			// 计数
			if (tClass.isInstance(arg) && index != matchIndex) {
				matchIndex++;
			}
			else if (tClass.isInstance(arg) && index == matchIndex) {
				args[i] = replaceTarget;
			}
		}
	}

	/**
	 * 获取该类的参数
	 * @param point 执行点
	 * @return K:Class对象 | V:参数值
	 */
	public static <T> T getRequestParamByClazz(ProceedingJoinPoint point, Class<T> tClass, int index) {
		AtomicInteger counter = new AtomicInteger();
		Map<Class<?>, Object> requestParam = getParameters(point, (before, after) -> {
			counter.getAndIncrement();
			if (Objects.equals(counter.get(), index)) {
				return after;
			}
			return before;
		});
		for (Map.Entry<Class<?>, Object> classObjectEntry : requestParam.entrySet()) {
			if (tClass.isAssignableFrom(classObjectEntry.getKey())) {
				return (T) classObjectEntry.getValue();
			}
		}
		return null;
	}

	/**
	 * 获取该类的参数
	 * @param point 执行点
	 * @return K:Class对象 | V:参数值
	 */
	public static Object getRequestParamByName(ProceedingJoinPoint point, String name) {
		return getCanJsonParameters(point).get(name);
	}

	/**
	 * 获取该类的参数
	 * @param point 执行点
	 * @param mergeFunction 冲突处理函数
	 * @return K:Class对象 | V:参数值
	 */
	public static Map<Class<?>, Object> getParameters(ProceedingJoinPoint point, BinaryOperator<Object> mergeFunction) {
		return Arrays.stream(point.getArgs())
				.collect(Collectors.toMap(Object::getClass, Function.identity(), mergeFunction));
	}

	/**
	 * 获取方法参数
	 * @param point 执行点
	 * @return K:参数名 | V:参数值
	 */
	public static Map<String, Object> getParameters(ProceedingJoinPoint point) {
		MethodSignature ms = (MethodSignature) point.getSignature();
		// 请求参数处理
		final Map<String, Object> paraMap = new HashMap<>(16);
		String[] params = ms.getParameterNames();
		Object[] args = point.getArgs();
		for (int i = 0; i < params.length; i++) {
			String param = params[i];
			Object arg = args[i];
			paraMap.put(param, arg);
		}
		return paraMap;
	}

	/**
	 * 获取方法参数
	 * @param point 执行点
	 * @return K:参数名 | V:参数值
	 */
	public static Map<String, Object> getCanJsonParameters(ProceedingJoinPoint point) {
		MethodSignature ms = (MethodSignature) point.getSignature();
		// 请求参数处理
		final Map<String, Object> paraMap = new HashMap<>(16);
		String[] params = ms.getParameterNames();
		Object[] args = point.getArgs();

		for (int i = 0; i < args.length; i++) {
			// 读取方法参数
			assert params != null;
			String parameterName = params[i];
			Object value = args[i];
			// 处理 参数
			if (value instanceof HttpServletRequest) {
				paraMap.putAll(((HttpServletRequest) value).getParameterMap());
			}
			else if (value instanceof WebRequest) {
				paraMap.putAll(((WebRequest) value).getParameterMap());
			}
			else if (value instanceof MultipartFile) {
				MultipartFile multipartFile = (MultipartFile) value;
				String name = multipartFile.getName();
				String fileName = multipartFile.getOriginalFilename();
				paraMap.put(name, fileName);
			}
			else if (value instanceof HttpServletResponse || value instanceof InputStream
					|| value instanceof InputStreamSource) {
			}
			else if (value instanceof Class) {
				paraMap.put(parameterName, String.format("Class:%s", ((Class<?>) value).getName()));
			}
			else if (value instanceof List) {
				List<?> list = (List<?>) value;
				AtomicBoolean isSkip = new AtomicBoolean(false);
				for (Object o : list) {
					if ("StandardMultipartFile".equalsIgnoreCase(o.getClass().getSimpleName())) {
						continue;
					}
				}
				// 加入参数
				if (isSkip.get()) {
					paraMap.put(parameterName, "此参数不能序列化为json");
				}
				else {
					paraMap.put(parameterName, list);
				}
			}
			else {
				paraMap.put(parameterName, value);
			}
		}
		return paraMap;
	}

	/**
	 * cglib 获取非代理Class
	 */
	public static Class cglibGetNoProxyClass(Class proxyClass) {
		while (proxyClass.getName().contains("CGLIB")) {
			proxyClass = proxyClass.getSuperclass();
		}
		return proxyClass;
	}

	/**
	 * 取代当前代理对象
	 * @param target 需要代理的对象
	 * @param beanFactory bean工厂，保证从spring环境获取实例
	 * @param adviceClasses 需要应用的aspect类
	 * @return 代理对象
	 */
	public static <E> E replaceProxy(E target, BeanFactory beanFactory, @NonNull Class<?>... adviceClasses) {
		AspectJProxyFactory aspectJProxyFactory = new AspectJProxyFactory(target);
		for (Class<?> adviceClass : adviceClasses) {
			Object bean = beanFactory.getBean(adviceClass);
			aspectJProxyFactory.addAspect(bean);
		}
		return aspectJProxyFactory.getProxy();
	}

	/**
	 * 获取可见字符串
	 */
	public static String toShowString(Object object) {
		if (Objects.isNull(object)) {
			return "null";
		}

		String result;
		try {
			result = JacksonTool.obj2String(object);
			if (StringUtils.isEmpty(result) || Objects.equals(result, "{}")) {
				result = object.toString();
			}
		}
		catch (Exception exception) {
			log.error("获取可见字符串异常", exception);
			result = "{}";
		}
		return result;
	}

}
