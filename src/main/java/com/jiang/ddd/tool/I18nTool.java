package com.jiang.ddd.tool;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * 国际化工具类
 */
@Slf4j
@Component
public class I18nTool {

	private static MessageSource messageSource;

	@Autowired
	public I18nTool(MessageSource messageSource) {
		I18nTool.messageSource = messageSource;
	}

	/**
	 * 获取多语言
	 * @param messageKey 消息key
	 * @param args 参数
	 */
	public static String getMessage(String messageKey, Object... args) {
		return getMessage(messageKey, LocaleContextHolder.getLocale(), args);
	}

	/**
	 * 获取多语言
	 * @param messageKey 消息key
	 * @param language 语言
	 * @param args 参数
	 */
	public static String getMessageByLanguage(String messageKey, String language, Object... args) {
		return getMessage(messageKey, new Locale(language), args);
	}

	private static String getMessage(String messageKey, Locale locale, Object... args) {
		String message = null;
		try {
			log.debug("当前语言是:{}", locale);
			message = messageSource.getMessage(messageKey, args, locale);
		}
		catch (NoSuchMessageException e) {
			log.info("No message found under code '{}' for locale '{}'.", messageKey, locale.getLanguage());
		}
		return message;
	}

}
