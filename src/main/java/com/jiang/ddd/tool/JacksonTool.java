package com.jiang.ddd.tool;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Slf4j
public class JacksonTool {

	/**
	 * 给个默认，本地能跑
	 */
	private static ObjectMapper objectMapper = new ObjectMapper();

	@Autowired
	public void setObjectMapper(ObjectMapper objectMapper) {
		JacksonTool.objectMapper = objectMapper;
	}

    private static XmlMapper xmlMapper = new XmlMapper();


    /**
     * 对象转xml格式字符串
     * @param obj 对象
     * @return Json格式字符串
     */
    public static <T> String obj2XMLStr(T obj) {
        if (obj == null) {
            return null;
        }
        try {
            return obj instanceof String ? (String) obj : xmlMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            log.warn("Parse Object to String error : {}", e.getMessage());
            return null;
        }
    }

    /**
     * 对象转Json格式字符串
     * @param obj 对象
     * @return Json格式字符串
     */
    public static <T> String obj2String(T obj) {
        if (obj == null) {
            return null;
        }
        try {
            return obj instanceof String ? (String) obj : objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            log.warn("Parse Object to String error : {}", e.getMessage());
            return null;
        }
    }

	/**
	 * 对象转Json格式字符串(格式化的Json字符串)
	 * @param obj 对象
	 * @return 美化的Json格式字符串
	 */
	public static <T> String obj2StringPretty(T obj) {
		if (obj == null) {
			return null;
		}
		try {
			return obj instanceof String ? (String) obj
					: objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
		}
		catch (JsonProcessingException e) {
			log.warn("Parse Object to String error : {}", e.getMessage());
			return null;
		}
	}

	/**
	 * 字符串转换为自定义对象
	 * @param str 要转换的字符串
	 * @param clazz 自定义对象的class对象
	 * @return 自定义对象
	 */
	public static <T> T string2Obj(String str, Class<T> clazz) {
		if (StringUtils.isEmpty(str) || clazz == null) {
			return null;
		}
		try {
			return clazz.equals(String.class) ? (T) str : objectMapper.readValue(str, clazz);
		}
		catch (Exception e) {
			log.warn("Parse String to Object error : {}", e.getMessage());
			return null;
		}
	}

	/**
	 * 通过referenceType反序列化
	 * @param str json
	 * @param typeReference 应用对象
	 * @param <T> 对象类型
	 * @return 对象，可以是集合
	 */
	public static <T> T string2Obj(String str, TypeReference<T> typeReference) {
		if (StringUtils.isEmpty(str) || typeReference == null) {
			return null;
		}
		try {
			return (T) (typeReference.getType().equals(String.class) ? str
					: objectMapper.readValue(str, typeReference));
		}
		catch (IOException e) {
			log.warn("Parse String to Object error", e);
			return null;
		}
	}

}
