package com.jiang.ddd.tool;

import com.jiang.ddd.business.base.BasicModel;
import com.jiang.ddd.constant.AppConstants;
import lombok.SneakyThrows;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;

/**
 * 获得model的工具类
 *
 * @author jiang.li
 * @since 2021/10/1
 */
@Component
public class ModelTool {

	@Resource
	private WebApplicationContext webApplicationContext;

	@SneakyThrows
	public <M extends BasicModel<?, ?>> M getModel(M model) {
		AutowireCapableBeanFactory factory = webApplicationContext.getAutowireCapableBeanFactory();

		// 注入SpringBean
		factory.autowireBean(model);

		// model初始化
		model.init();

		// 获取代理
		model = AOPTool.replaceProxy(model, factory, AppConstants.AOP.SERVICE_METHOD_ASPECTS);

		// 添加AOP
		return model;
	}

}
