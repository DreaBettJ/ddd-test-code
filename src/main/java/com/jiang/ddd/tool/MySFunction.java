package com.jiang.ddd.tool;

import java.io.Serializable;
import java.util.function.Function;

/**
 * 支持序列化的 Function
 */
@FunctionalInterface
public interface MySFunction<T, R> extends Function<T, R>, Serializable {

}