package com.jiang.ddd.tool;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Null 工具类
 *
 * @author jiang.li
 * @since 2021/8/27
 */
public class NullTool {

	/**
	 * 判断是否为空
	 * @param supplier 表达式
	 */
	public static boolean isNull(Supplier<?> supplier) {
		try {
			return Objects.isNull(supplier.get());
		}
		catch (NullPointerException e) {
			return true;
		}
	}

	/**
	 * 是否为true
	 * @param supplier 表达式
	 */
	public static boolean isTrue(Supplier<Boolean> supplier) {
		try {
			return Boolean.TRUE.equals(supplier.get());
		}
		catch (NullPointerException e) {
			return false;
		}
	}

	/**
	 * 所有是否为true
	 * @param suppliers 表达式集
	 */
	@SafeVarargs
	public static boolean isAllTrue(Supplier<Boolean>... suppliers) {
		return Arrays.stream(suppliers).allMatch(NullTool::isTrue);
	}

	/**
	 * 任意是否为true
	 * @param suppliers 表达式集
	 */
	@SafeVarargs
	public static boolean isAnyTrue(Supplier<Boolean>... suppliers) {
		return Arrays.stream(suppliers).anyMatch(NullTool::isTrue);
	}

	/**
	 * 获取流
	 * @param supplier 表达式
	 */
	public static <T> Stream<T> stream(Supplier<Collection<T>> supplier) {
		try {
			return supplier.get().stream();
		}
		catch (NullPointerException e) {
			return Stream.empty();
		}
	}

	/**
	 * 判断是否为不空
	 * @param supplier 表达式
	 */
	public static boolean nonNull(Supplier<?> supplier) {
		return !isNull(supplier);
	}

	/**
	 * 对 Optional 封装
	 */
	public static <S> Optional<S> of(Supplier<S> supplier) {
		try {
			return Optional.ofNullable(supplier.get());
		}
		catch (NullPointerException | IndexOutOfBoundsException e) {
			return Optional.empty();
		}
	}

}
