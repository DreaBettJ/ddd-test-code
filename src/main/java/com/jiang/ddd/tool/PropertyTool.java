package com.jiang.ddd.tool;

import com.jiang.ddd.business.base.BasicEntity;
import lombok.SneakyThrows;

import java.lang.invoke.SerializedLambda;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * 获得属性的工具类 PropertyTool.getProperty(User::getName) --> name
 *
 * @author jiang.li
 * @since 2021/8/8
 */
public class PropertyTool  {

	/**
	 * 获取属性的缓存，减少序列化时间消耗
	 */
	private static final Map<MySFunction<?,?>,String> cacheMap = new HashMap<>();

	/**
	 * 根据 get 的 lambda 表达式返回属性名
	 */
	public static <T> String getProperty(MySFunction<T, Object> sFunction) {
		if (cacheMap.containsKey(sFunction)){
			return cacheMap.get(sFunction);
		} else {
			SerializedLambda serializedLambda = getSerializedLambda(sFunction);
			String property = methodToProperty(serializedLambda.getImplMethodName());
			cacheMap.put(sFunction,property);
			return property;
		}
	}

	/**
	 * 获取序列的lambda，读取信息
	 */
	@SneakyThrows
	private static <T> SerializedLambda getSerializedLambda(MySFunction<T, Object> sFunction) {
		Method method = sFunction.getClass().getDeclaredMethod("writeReplace");
		method.setAccessible(true);
		return (SerializedLambda) method.invoke(sFunction);
	}

	/**
	 * 方法名转字段名，直接copy mp的代码 其实就是按java bean的规范，先把get、set、is前缀去掉，然后第二个字符如果不是大写，就把第一个转小写
	 * @param name 方法名
	 * @return 字段名
	 */
	public static String methodToProperty(String name) {
		if (name.startsWith("is")) {
			name = name.substring(2);
		}
		else if (name.startsWith("get") || name.startsWith("set")) {
			name = name.substring(3);
		}
		else {
			throw new RuntimeException(
					"Error parsing property name '" + name + "'.  Didn't start with 'is', 'get' or 'set'.");
		}

		if (name.length() == 1 || (name.length() > 1 && !Character.isUpperCase(name.charAt(1)))) {
			name = name.substring(0, 1).toLowerCase(Locale.ENGLISH) + name.substring(1);
		}

		return name;
	}

	public static void main(String[] args) {
		ReflectInfoTool reflectInfoTool = new ReflectInfoTool();
		getProperty(BasicEntity::getCreateTime);
//		reflectInfoTool.getClassInPackageAndExtendClass()
	}
}
