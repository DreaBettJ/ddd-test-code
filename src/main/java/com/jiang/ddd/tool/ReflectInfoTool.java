package com.jiang.ddd.tool;

import cn.hutool.core.util.ArrayUtil;
import com.jiang.ddd.aop.extend.SelfInterface;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ScannedGenericBeanDefinition;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.MethodParameter;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.core.annotation.SynthesizingMethodParameter;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.TypeFilter;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;
import org.springframework.web.context.support.StandardServletEnvironment;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * 反射工具类
 *
 * @author jiang.li
 * @since 2021/8/28
 */
@Slf4j
@Component
public class ReflectInfoTool implements SelfInterface<ReflectInfoTool> {

	/**
	 * 通过过滤器获得该类下所有的属性，包括私有的
	 * @param clazz 指定类
	 * @param fieldFilter 过滤器
	 * @return 属性集合
	 */
	public List<Field> getAllField(Class<?> clazz, Predicate<? super Field> fieldFilter) {
		List<Field> fields = new ArrayList<>();
		// 不是Object
		while (!Object.class.getName().equals(clazz.getName())) {
			List<Field> filedFields = Arrays.stream(clazz.getDeclaredFields()).filter(fieldFilter)
					.collect(Collectors.toList());
			fields.addAll(filedFields);
			clazz = clazz.getSuperclass();
		}
		return fields;
	}

	/**
	 * 获得该类下所有的属性，包括私有的
	 * @param clazz 指定类
	 * @return 属性集合
	 */
	public List<Field> getAllField(Class<?> clazz) {
		return self().getAllField(clazz, (field) -> true);
	}

	/**
	 * 获得该类下所有的被注解的属性，包括私有的
	 * @param clazz 指定类
	 * @param annotationClasses 注解类集合
	 * @return 属性集合
	 */
	public List<Field> getAllField(Class<?> clazz, Class<?>... annotationClasses) {
		return self().getAllField(clazz, (field) -> {
			List<? extends Class<? extends Annotation>> collect = Arrays.stream(field.getAnnotations())
					.map(Annotation::annotationType).collect(Collectors.toList());
			return Arrays.stream(annotationClasses).anyMatch(collect::contains);
		});
	}

	/**
	 * 获取指定名称的field
	 * @param fieldName 属性名
	 * @return 属性集合
	 */
	public <T> T getFieldByName(Object target, String fieldName, Class<T> fieldClazz) {
		Optional<Field> fieldOptional = Arrays.stream(target.getClass().getDeclaredFields())
				.filter(field -> Objects.equals(field.getName(), fieldName)).findFirst();
		if (fieldOptional.isPresent()) {
			Field field = fieldOptional.get();
			field.setAccessible(true);
			try {
				return fieldClazz.cast(field.get(target));
			}
			catch (IllegalAccessException e) {
				return null;
			}
		}
		else {
			return null;
		}
	}

	/**
	 * 获取get方法
	 * @param fieldName 属性名
	 * @param cls class
	 * @return 方法
	 */
	public Method getGetter(String fieldName, Class<?> cls) {
		for (Method method : cls.getMethods()) {
			if (method.getName().equalsIgnoreCase("get".concat(fieldName)) && method.getParameterTypes().length == 0) {
				return method;
			}
		}
		return null;
	}

	/**
	 * 获取set方法
	 * @param fieldName 属性名
	 * @param cls class
	 * @return 方法
	 */
	public Method getSetter(String fieldName, Class<?> cls) {
		for (Method method : cls.getMethods()) {
			if (method.getName().equalsIgnoreCase("set".concat(fieldName))) {
				return method;
			}
		}
		return null;
	}

	private final ParameterNameDiscoverer PARAMETER_NAME_DISCOVERER = new DefaultParameterNameDiscoverer();

	/**
	 * 获取方法参数信息
	 * @param constructor 构造器
	 * @param parameterIndex 参数序号
	 * @return {MethodParameter}
	 */
	public MethodParameter getMethodParameter(Constructor<?> constructor, int parameterIndex) {
		MethodParameter methodParameter = new SynthesizingMethodParameter(constructor, parameterIndex);
		methodParameter.initParameterNameDiscovery(PARAMETER_NAME_DISCOVERER);
		return methodParameter;
	}

	/**
	 * 获取方法参数信息
	 * @param method 方法
	 * @param parameterIndex 参数序号
	 * @return {MethodParameter}
	 */
	public MethodParameter getMethodParameter(Method method, int parameterIndex) {
		MethodParameter methodParameter = new SynthesizingMethodParameter(method, parameterIndex);
		methodParameter.initParameterNameDiscovery(PARAMETER_NAME_DISCOVERER);
		return methodParameter;
	}

	/**
	 * 获取注解参数索引
	 * @param method 方法名称
	 * @param annotationClass 注解类
	 * @return 参数索引
	 */
	@SneakyThrows
	public int indexParameterByAnnotation(Method method, Class<? extends Annotation> annotationClass) {
		for (int i = 0; i < method.getParameterCount(); i++) {
			Parameter parameter = method.getParameters()[i];
			Annotation annotation = parameter.getAnnotation(annotationClass);
			if (Objects.nonNull(annotation)) {
				return i;
			}
		}
		return -1;
	}

	@SneakyThrows
	public List<MethodParameter> getMethodParametersByAnnotation(Method method,
			Class<? extends Annotation> annotationClass) {
		List<MethodParameter> parameters = new ArrayList<>();
		for (int i = 0; i < method.getParameterCount(); i++) {
			MethodParameter methodParameter = new SynthesizingMethodParameter(method, i);
			methodParameter.initParameterNameDiscovery(PARAMETER_NAME_DISCOVERER);
			Annotation methodAnnotation = methodParameter.getMethodAnnotation(annotationClass);
			if (Objects.nonNull(methodAnnotation)) {
				parameters.add(methodParameter);
			}
		}
		return parameters;
	}

	/**
	 * 获得父类泛型类型的class
	 * @param clazz 目标class
	 * @param index 泛型索引
	 * @return 泛型类型的class
	 */
	public Class<?> getClassActualType(Class<?> clazz, int index) {
		Type genericSuperclass = clazz.getGenericSuperclass();
		if (genericSuperclass instanceof ParameterizedType) {
			Type[] actualTypeArguments = ((ParameterizedType) genericSuperclass).getActualTypeArguments();
			if (Objects.nonNull(actualTypeArguments[index])) {
				return (Class<?>) actualTypeArguments[index];
			}
		}
		return null;
	}

	/**
	 * 获得接口泛型类型的class
	 * @param clazz 目标class
	 * @param interfaceIndex 接口索引
	 * @param typeIndex 泛型索引
	 * @return 泛型类型的class
	 */
	public Class<?> getInterfaceActualType(Class<?> clazz, int interfaceIndex, int typeIndex) {
		Type[] genericInterfaces = clazz.getGenericInterfaces();
		Type genericInterface = genericInterfaces[interfaceIndex];
		if (genericInterface instanceof ParameterizedType) {
			Type[] actualTypeArguments = ((ParameterizedType) genericInterface).getActualTypeArguments();
			if (Objects.nonNull(actualTypeArguments[typeIndex])) {
				return (Class<?>) actualTypeArguments[typeIndex];
			}
		}
		return null;
	}

	/**
	 * 执行静态方法
	 * @param clazz 类
	 * @return 执行结果
	 */
	@SneakyThrows
	public <T> T getStaticField(Class<?> clazz, String fieldName, Class<T> targetType) {
		Field declaredField = clazz.getDeclaredField(fieldName);
		declaredField.setAccessible(true);
		return targetType.cast(declaredField.get(clazz));
	}

	/**
	 * 扫描路径下的所有class
	 * @return
	 */
	public List<Class<?>> getClassInPackage(String... basePackages) {
		TypeFilter anyTypeFilter = (metadataReader, metadataReaderFactory) -> true;
		return self().getClassInPackage(Collections.singletonList(anyTypeFilter), Collections.emptyList(),
				basePackages);
	}

	/**
	 * 扫描路径下的所有class,并且继承该类
	 * @return
	 */
	public List<Class<?>> getClassInPackageAndExtendClass(Class superClazz, String... basePackages) {
		return self().getClassInPackage(Collections.singletonList(new SubClassFilter<>(superClazz)),
				Collections.emptyList(), basePackages);
	}

	private final ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();

	private final MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory();

	/**
	 * 获取路径下的所有类
	 * @param includeTypeFilters 包括过滤器
	 * @param excludeTypeFilters 排除过滤器
	 * @param basePackages eg: com.xxx.xxx
	 * @return 反射类对象集合
	 */
	@SneakyThrows
	public List<Class<?>> getClassInPackage(List<TypeFilter> includeTypeFilters, List<TypeFilter> excludeTypeFilters,
			String... basePackages) {
		// 扫描class
		List<BeanDefinition> beanDefinitions = Arrays.stream(basePackages).map(self()::packageToPath).map(path -> {
			try {
				return resourcePatternResolver.getResources(path);
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}).filter(Objects::nonNull).flatMap(Arrays::stream).filter(Resource::isReadable).map(resource -> {
			try {
				return metadataReaderFactory.getMetadataReader(resource);
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}).filter(Objects::nonNull)
				.filter(metadataReader -> isMatch(metadataReader, includeTypeFilters, excludeTypeFilters))
				.map(ScannedGenericBeanDefinition::new).collect(Collectors.toList());

		// 使用反射
		ArrayList<Class<?>> classes = new ArrayList<>();
		for (BeanDefinition beanDefinition : beanDefinitions) {
			classes.add(Class.forName(beanDefinition.getBeanClassName()));
		}
		return classes;
	}

	/**
	 * 是否匹配过滤器
	 */
	@SneakyThrows
	protected boolean isMatch(MetadataReader metadataReader, List<TypeFilter> includeFilters,
			List<TypeFilter> excludeFilters) {
		for (TypeFilter tf : excludeFilters) {
			if (tf.match(metadataReader, metadataReaderFactory)) {
				return false;
			}
		}
		for (TypeFilter tf : includeFilters) {
			if (tf.match(metadataReader, metadataReaderFactory)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 类路径转扫描路径
	 */
	public String packageToPath(String basePackage) {
		String DEFAULT_RESOURCE_PATTERN = "**/*.class";
		return ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + resolveBasePackage(basePackage) + '/'
				+ DEFAULT_RESOURCE_PATTERN;
	}

	/**
	 * package和path转化
	 */
	private String resolveBasePackage(String basePackage) {
		return ClassUtils.convertClassNameToResourcePath(
				new StandardServletEnvironment().resolveRequiredPlaceholders(basePackage));
	}

	/**
	 * 获取方法
	 * @param clazz 类
	 * @param methodName 方法名
	 * @param args 参数
	 * @throws NoSuchMethodException 找不到对应方法
	 */
	@SneakyThrows
	public Method getMethod(Class<?> clazz, String methodName, Object... args) {
		Class<Object>[] classes = new Class[args.length];
		if (ArrayUtil.isNotEmpty(args)) {
			for (int i = 0; i < args.length; i++) {
				classes[i] = (Class<Object>) NullTool.of(args[i]::getClass).orElse(null);
			}
		}
		Method[] methods = clazz.getDeclaredMethods();
		Method method = Arrays.stream(methods).filter(met -> Objects.equals(methodName, met.getName()))
				.filter(met -> isParameterTypeMatch(met.getParameterTypes(), classes)).findFirst()
				.orElseThrow(NoSuchMethodException::new);
		return method;
	}

	/**
	 * 获取构造器
	 */
	@SneakyThrows
	public Optional<Constructor<?>> getContractor(Class<?> clazz, Object... args) {
		Class<Object>[] classes = new Class[args.length];
		if (ArrayUtil.isNotEmpty(args)) {
			for (int i = 0; i < args.length; i++) {
				classes[i] = (Class<Object>) NullTool.of(args[i]::getClass).orElse(null);
			}
		}
		Constructor<?>[] constructors = clazz.getDeclaredConstructors();
		return Arrays.stream(constructors).filter(met -> isParameterTypeMatch(met.getParameterTypes(), classes))
				.findFirst();
	}

	/**
	 * 参数class数组匹配对比
	 * @param parameterClasses 参数
	 * @param matchClasses 被比较
	 */
	public boolean isParameterTypeMatch(Class<?>[] parameterClasses, Class<?>[] matchClasses) {
		if (parameterClasses.length != matchClasses.length) {
			return false;
		}
		for (int i = 0; i < parameterClasses.length; i++) {
			// 跳过空
			if (Objects.isNull(matchClasses[i])) {
				continue;
			}
			// 对比是否子类
			if (!parameterClasses[i].isAssignableFrom(matchClasses[i])) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 有参构造
	 */
	public <T> T haveArgInstance(Class<T> clazz, Object... args) {
		Optional<Constructor<?>> contractor = getContractor(clazz, args);
		if (!contractor.isPresent()) {
			return null;
		}
		try {
			return clazz.cast(contractor.get().newInstance(args));
		}
		catch (InvocationTargetException | InstantiationException | IllegalAccessException e) {
			return null;
		}
	}

	public Map<String, Class> getClassInfo(List<Class<?>> classList) {
		return classList.stream().collect(Collectors.toMap(Class::getSimpleName, Function.identity()));
	}

	/**
	 * 获取class信息，携带field {Class -> [{field1 -> [field1Anno1,field1Anno2]}]}
	 * @param classList 类对象集合
	 */
	public Map<Class, Map<Field, List<Annotation>>> getClassFieldInfoMap(List<Class> classList) {
		return classList.stream().collect(Collectors.toMap(Function.identity(), clazz -> {
			// 第一层value
			List<Field> allField = self().getAllField(clazz);
			return allField.stream()
					.collect(Collectors.toMap(Function.identity(), field -> Arrays.asList(field.getAnnotations())));
		}));
	}

	/**
	 * 获取class信息，携带field {Class -> [{field1 -> [field1Anno1,field1Anno2]}]}
	 * @param classList 类对象集合
	 */
	public Map<String, Map<String, List<Annotation>>> getClassFieldNameAnnoInfoMap(List<Class> classList) {
		return classList.stream().collect(Collectors.toMap(Class::getSimpleName, clazz -> {
			// 第一层value
			List<Field> allField = self().getAllField(clazz);
			return allField.stream()
					.collect(Collectors.toMap(Field::getName, field -> Arrays.asList(field.getAnnotations())));
		}));
	}

	/**
	 * 获取class信息，携带field {ClassName -> [{field1Name -> field1}]}
	 * @param classList 类对象集合
	 */
	public Map<String, Map<String, Field>> getClassFieldNameInfoMap(List<Class<?>> classList) {
		return classList.stream().collect(Collectors.toMap(Class::getSimpleName, clazz -> {
			// 第一层value
			List<Field> allField = self().getAllField(clazz);
			return allField.stream().collect(Collectors.toMap(Field::getName, Function.identity()));
		}));
	}

	/**
	 * 获取class信息 {Class -> [classAnno1,classAnno2]}
	 * @param classList 类对象集合
	 */
	public <T> Map<Class<T>, List<Annotation>> getClassAnnoInfo(List<Class<T>> classList) {
		return classList.stream()
				.collect(Collectors.toMap(Function.identity(), clazz -> Arrays.asList(clazz.getAnnotations())));
	}

	/**
	 * 子类过滤器
	 */
	public static class SubClassFilter<T> implements TypeFilter {

		private final Class<T> targetClass;

		public SubClassFilter(Class<T> targetClass) {
			this.targetClass = targetClass;
		}

		@Override
		@SneakyThrows
		public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory)
				throws IOException {
			return targetClass.isAssignableFrom(Class.forName(metadataReader.getClassMetadata().getClassName()));
		}

	}

}
