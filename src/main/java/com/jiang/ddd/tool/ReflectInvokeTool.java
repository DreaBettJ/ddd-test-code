package com.jiang.ddd.tool;

import cn.hutool.core.convert.Convert;
import com.jiang.ddd.aop.extend.SelfInterface;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Objects;

@Component
public class ReflectInvokeTool implements SelfInterface<ReflectInvokeTool> {

	@Resource
	ReflectInfoTool reflectInfoTool;

	@SneakyThrows
	public Object invokeSetter(String fieldName, Object fieldValue, Object obj) {
		Method setter = reflectInfoTool.getSetter(fieldName, obj.getClass());
		if (setter == null) {
			throw new ReflectiveOperationException("没有set方法");
		}
		return setter.invoke(obj, fieldValue);
	}

	/**
	 * 执行静态方法
	 * @param clazz 类
	 * @param methodName 方法名
	 * @param args 参数
	 * @return 执行结果
	 */
	@SneakyThrows
	public Object invokeStaticMethod(Class<?> clazz, String methodName, Object... args) {
		Method method = clazz.getMethod(methodName, Object.class);
		method.setAccessible(true);
		return method.invoke(null, args);
	}

	/**
	 * 无参构造
	 */
	public <T> T noArgInstance(Class<T> clazz) {
		try {
			return clazz.newInstance();
		}
		catch (InstantiationException | IllegalAccessException e) {
			return null;
		}
	}

	/**
	 * field 生成
	 */
	public Object createObjectByFields(Map<String, String> fieldMap, Map<String, Field> stringFieldMap, Class clazz) {
		Object entity = self().noArgInstance(clazz);
		if (Objects.nonNull(stringFieldMap)) {
			fieldMap.forEach((key, value) -> {
				String fieldName = StringTool.toObjectNameForm(key);
				// 获取field
				Field field = stringFieldMap.get(fieldName);
				Class<?> type = field.getType();
				String fieldStr = fieldMap.get(fieldName);
				self().invokeSetter(fieldName, Convert.convert(type, fieldStr), entity);
			});
		}
		return entity;
	}

	/**
	 * 调用方法
	 * @param clazz 类
	 * @param methodName 方法名
	 * @param invoker 调用者
	 * @param args 参数
	 * @return 调用结果
	 */
	@SneakyThrows
	public Object invokeMethod(Class<?> clazz, String methodName, Object invoker, Object... args) {
		return reflectInfoTool.getMethod(clazz, methodName, args).invoke(invoker, args);
	}

	@SneakyThrows
	public Object getValueByGetter(String fieldName, Object obj) {
		Method getter = reflectInfoTool.getGetter(fieldName, obj.getClass());
		if (getter != null) {
			return getter.invoke(obj);
		}
		return null;
	}

}
