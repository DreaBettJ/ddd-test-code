package com.jiang.ddd.tool;

import java.util.Objects;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 正则工具类
 *
 * @author jiang.li
 * @since 2021/12/4
 */
public class RegexTool {

	/**
	 * 手机号
	 */
	public static final String REGEX_PHONE = "(?:17[0-9]|14[0-9]|13[0-9]|15[^4,\\D]|18[0,5-9])\\d{8}";

	/**
	 * 邮箱
	 */
	public static final String REGEX_EMAIL = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";

	/**
	 * 汉字
	 */
	public static final String REGEX_CHINESE = "[\u4e00-\u9fa5]*";

	/**
	 * 身份证
	 */
	public static final String REGEX_ID_CARD = "[1-9]\\d{5}(?:18|19|[23]\\d)\\d{2}(?:0[1-9]|(?:10|11|12))(?:[0-2][1-9]|10|20|30|31)\\d{3}[0-9Xx]|[1-9]\\d{5}\\d{2}(?:0[1-9]|(?:10|11|12))(?:[0-2][1-9]|10|20|30|31)\\d{2}";

	/**
	 * URL
	 */
	public static final String REGEX_URL = "http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&=]*)?";

	/**
	 * IP地址
	 */
	public static final String REGEX_IP_ADDR = "(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)";

	/**
	 * 按照规则替换所有
	 */
	public static String replaceAllByRule(String regex, String input, Function<String, String> replaceRule) {
		Matcher matcher = getMatcher(regex, input);
		StringBuffer stringBuilder = new StringBuffer();
		while (matcher.find()) {
			String matched = input.substring(matcher.start(), matcher.end());
			String replace = replaceRule.apply(matched);
			matcher.appendReplacement(stringBuilder, replace);
		}
		matcher.appendTail(stringBuilder);
		return stringBuilder.toString();
	}

	/**
	 * 替换所有
	 */
	public static String replaceAll(String regex, String input, String replace) {
		Matcher matcher = getMatcher(regex, input);
		return matcher.replaceAll(replace);
	}

	/**
	 * 获得匹配器
	 */
	public static Matcher getMatcher(String regex, String input) {
		Pattern pattern = Pattern.compile(regex);
		return pattern.matcher(input);
	}

	/**
	 * 返回匹配个数
	 */
	public static String find(String regex, String input, int index) {
		Matcher matcher = getMatcher(regex, input);
		int count = 0;
		while (matcher.find()) {
			if (Objects.equals(count, index)) {
				return input.substring(matcher.start(), matcher.end());
			}
			count++;
		}
		return null;
	}

	/**
	 * 返回匹配个数
	 */
	public static int containCount(String regex, String input) {
		Matcher matcher = getMatcher(regex, input);
		int count = 0;
		while (matcher.find()) {
			count++;
		}
		return count;
	}

	/**
	 * 是否包含
	 */
	public static boolean contain(String regex, String input) {
		return Pattern.matches(getAllMatchRegex(regex), input);
	}

	/**
	 * 由原来的自增获取匹配所有的正则
	 */
	public static String getAllMatchRegex(String regex) {
		return String.format("(^[\\S\\s]*\\b)(%s)(\\b[\\S\\s]*$)", regex);
	}

	/**
	 * 脱敏方法
	 * @param regex 正则
	 * @param input 输入字符
	 */
	public static String desensitize(String regex, String input, int beginLength, int endLength) {
		return replaceAllByRule(regex, input, str -> {
			int starLength = str.length() - beginLength - endLength;
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(str, 0, beginLength);
			for (int i = 0; i < starLength; i++) {
				stringBuilder.append("*");
			}
			stringBuilder.append(str, str.length() - endLength, str.length());
			return stringBuilder.toString();
		});
	}

}
