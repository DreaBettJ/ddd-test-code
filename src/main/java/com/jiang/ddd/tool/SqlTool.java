package com.jiang.ddd.tool;

import java.util.HashMap;
import java.util.Map;

public class SqlTool {

	/**
	 * 解析表名
	 */
	public static String getTableName(String sql) {
		String method = StringTool.getWord(sql, 1).toLowerCase().trim();
		int tableIndex;
		switch (method) {
		case "insert":
		case "delete":
			tableIndex = 3;
			break;
		case "update":
			tableIndex = 2;
			break;
		case "select":
			tableIndex = 4;
			break;
		default:
			return "";
		}
		return StringTool.getWord(sql, tableIndex);
	}

	/**
	 * 解析属性
	 */
	public static Map<String, String> getFieldMap(String sql) {
		sql = sql.toLowerCase();
		sql = sql.trim();
		sql = sql.replace("\n", " ");
		sql = sql.replace("\r\n", " ");
		sql = sql.replace("'", "");
		sql = StringTool.beautySpace(sql);
		String method = StringTool.getWord(sql, 1);
		HashMap<String, String> result = new HashMap<>();
		switch (method) {
		case "insert":
			String fieldNameStr = StringTool.betweenString(sql, "(", ")", 1);
			String fieldValueStr = StringTool.betweenString(sql, "(", ")", 2);

			String[] fieldNames = fieldNameStr.split(",");
			String[] fieldValues = fieldValueStr.split(",");

			for (int i = 0; i < fieldNames.length; i++) {
				result.put(StringTool.toObjectNameForm(fieldNames[i].trim()), fieldValues[i].trim());
			}
			break;
		case "update":
			String subSql = StringTool.getAfter(sql, "set");
			subSql = StringTool.getBefore(subSql, "where");
			String[] updateFieldStrs = subSql.split(",");
			for (String updateFieldStr : updateFieldStrs) {
				String[] split = updateFieldStr.split("=");
				result.put(StringTool.toObjectNameForm(split[0].trim()), split[1].trim());
			}
			break;
		}
		return result;
	}

	/**
	 * 查询sql属性
	 */
	public static Map<String, String> getFieldMapWithObjectForm(String sql) {
		Map<String, String> fieldMap = getFieldMap(sql);
		HashMap<String, String> resultMap = new HashMap<>();
		fieldMap.forEach((key, value) -> {
			resultMap.put(StringTool.toObjectNameForm(key), value);
		});
		return resultMap;
	}

}
