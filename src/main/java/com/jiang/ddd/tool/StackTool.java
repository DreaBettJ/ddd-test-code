package com.jiang.ddd.tool;

import java.util.Arrays;
import java.util.function.Predicate;

/**
 * 堆栈工具类
 *
 * @author jiang.li
 * @since 2021/11/14
 */
public class StackTool {

	/**
	 * 获取上一个符合条件的调用栈
	 * @param throwable 抛错对象
	 * @param stackTraceElementPredicate 条件描述
	 */
	public static StackTraceElement getLastStack(Throwable throwable,
			Predicate<StackTraceElement> stackTraceElementPredicate) {
		StackTraceElement[] stackTrace = throwable.getStackTrace();

		// 过滤
		return NullTool.stream(() -> Arrays.asList(stackTrace)).filter(stackTraceElementPredicate).findFirst()
				.orElse(null);
	}

	/**
	 * 获取符合在包内的上一个堆栈信息
	 * @param throwable 抛错对象
	 * @param packages 包对象集合
	 */
	public static StackTraceElement getLastStackInPackages(Throwable throwable, String... packages) {
		return getLastStack(throwable, (stackTraceElement) -> NullTool.stream(() -> Arrays.asList(packages))
				.anyMatch(pck -> stackTraceElement.getClassName().contains(pck)));
	}

	/**
	 * 获取符合在包内的上一个堆栈信息
	 * @param throwable 抛错对象
	 * @param includePack 包括
	 * @param excludePack 排除
	 */
	public static StackTraceElement getLastStackInPackages(Throwable throwable, String includePack,
			String excludePack) {
		return getLastStack(throwable, (stackTraceElement) -> stackTraceElement.getClassName().contains(includePack)
				&& !stackTraceElement.getClassName().contains(excludePack));
	}

	/**
	 * 打印当前调用栈
	 */
	public static void printNowStack() {
		Throwable throwable = new Throwable();
		throwable.printStackTrace(System.out);
	}

}
