package com.jiang.ddd.tool;

import cn.hutool.core.util.CharUtil;

import java.util.Objects;
import java.util.function.Function;

/**
 * 字符串工具类
 *
 * @author jiang.li
 * @since 2021/11/14
 */
public class StringTool {

	/**
	 * 将驼峰转为 _ 分隔
	 */
	public static String toTableNameForm(String input) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < input.length(); i++) {
			char c = input.charAt(i);
			boolean isFirst = Objects.equals(0, i);
			if (CharUtil.isLetterUpper(c) && isFirst) {
				stringBuilder.append(String.format("%c", c).toLowerCase());
			}
			else if (CharUtil.isLetterUpper(c) && !isFirst) {
				stringBuilder.append(String.format("_%c", c).toLowerCase());
			}
			else {
				stringBuilder.append(c);
			}
		}
		return stringBuilder.toString();
	}

	/**
	 * 将 _ 分隔 转为 小驼峰
	 */
	public static String toObjectNameForm(String input) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < input.length(); i++) {
			char c = input.charAt(i);
			if (Objects.equals(c, '_')) {
				i++;
				c = input.charAt(i);
				stringBuilder.append(String.valueOf(c).toUpperCase());
			}
			else {
				stringBuilder.append(c);
			}
		}
		return stringBuilder.toString();
	}

	/**
	 * 将 _ 分隔 转为 小驼峰
	 */
	public static String toClassNameForm(String input) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < input.length(); i++) {
			char c = input.charAt(i);
			if (0 == i) {
				stringBuilder.append(String.valueOf(c).toUpperCase());
			}
			else if (Objects.equals(c, '_')) {
				i++;
				c = input.charAt(i);
				stringBuilder.append(String.valueOf(c).toUpperCase());
			}
			else {
				stringBuilder.append(c);
			}
		}
		return stringBuilder.toString();
	}

	/**
	 * 获取第 n 个单词
	 * @param input 输入
	 * @param number 第几个
	 */
	public static String getWord(String input, int number) {
		input = input.trim();
		String[] s = input.split(" ");
		if (number > s.length || number < 1) {
			return null;
		}
		return s[number - 1];
	}

	/**
	 * 获取中间字符串
	 */
	public static String betweenString(String input, String startSubstring, String endSubstring, int number) {
		try {
			int beginIndex = 0;
			int counter = 0;
			int endIndex = 0;
			int startIndex = 0;
			while (counter != number) {
				startIndex = input.indexOf(startSubstring, beginIndex) + startSubstring.length();
				endIndex = input.indexOf(endSubstring, startIndex);
				beginIndex = endIndex + 1;
				counter++;
			}
			return input.substring(startIndex, endIndex);
		}
		catch (Exception e) {
			throw new RuntimeException("错误的数量");
		}
	}

	/**
	 * 获取中间字符串,最短距离
	 */
	public static String betweenStringWithMinDistance(String input, String startSubstring, String endSubstring,
			int number) {
		try {
			int counter = 0;
			int endIndex = 0;
			int startIndex = 0;
			while (counter != number) {
				endIndex = input.indexOf(endSubstring, endIndex);
				startIndex = input.lastIndexOf(startSubstring, endIndex) + startSubstring.length();
				counter++;
			}
			return input.substring(startIndex, endIndex);
		}
		catch (Exception e) {
			throw new RuntimeException("错误的数量");
		}
	}

	/**
	 * 格式化空格
	 */
	public static String beautySpace(String input) {
		StringBuilder stringBuilder = new StringBuilder();
		char lastChar = 0;
		char c;
		for (int i = 0; i < input.length(); i++, lastChar = c) {
			c = input.charAt(i);
			if (Objects.equals(lastChar, ' ') && Objects.equals(c, ' ')) {
				continue;
			}
			stringBuilder.append(c);
		}
		return stringBuilder.toString();
	}

	/**
	 * 获取某个子串之后的
	 */
	public static String getAfter(String str, String substr) {
		int startIndex = str.indexOf(substr) + substr.length();
		return str.substring(startIndex);
	}

	/**
	 * 获取某个子串之后的
	 */
	public static String getBefore(String str, String substr) {
		int endIndex = str.indexOf(substr);
		return str.substring(0, endIndex);
	}

	/**
	 * 处理类似于 {key1}，{key2} 的占位符
	 * @param source 数据源模板
	 * @param process {key} 中的key作为入参，结果作为替换 {key}的字符串
	 */
	public static String replaceOccupancy(String source, Function<String, String> process) {
		StringBuilder stringBuilder = new StringBuilder();
		int beginIndex = -1;
		int endIndex;
		for (int i = 0; i < source.length(); i++) {
			char c = source.charAt(i);
			if (Objects.equals(c, '{')) {
				beginIndex = i + 1;
			}
			else if (Objects.equals(c, '}') && !Objects.equals(beginIndex, -1)) {
				endIndex = i;
				String substring = source.substring(beginIndex, endIndex);
				stringBuilder.append(process.apply(substring));
				beginIndex = -1;
			}
			else if (Objects.equals(beginIndex, -1)) {
				stringBuilder.append(c);
			}
		}
		return stringBuilder.toString();
	}

	public static String getAfterBack(String input, String subStr) {
		int i = input.lastIndexOf(subStr);
		if (i < input.length() - 1) {
			return input.substring(i + 1);
		}
		else {
			return null;
		}
	}

}
