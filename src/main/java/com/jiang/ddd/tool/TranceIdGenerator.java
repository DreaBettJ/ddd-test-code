package com.jiang.ddd.tool;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 链路id生成器
 *
 * @author jiang.li
 * @since 2021/9/6
 */
public class TranceIdGenerator {

	private static String IP_16;

	private static final AtomicInteger count = new AtomicInteger(1000);

	static {
		try {
			String ipAddress = Inet4Address.getLocalHost().getHostAddress();
			IP_16 = getIP_16(ipAddress);
		}
		catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	private static String getIP_16(String ip) {
		String[] ips = ip.split("\\.");
		StringBuilder sb = new StringBuilder();
		for (String column : ips) {
			String hex = Integer.toHexString(Integer.parseInt(column));
			if (hex.length() == 1) {
				sb.append('0').append(hex);
			}
			else {
				sb.append(hex);
			}
		}
		return sb.toString();
	}

	private static String getTraceId(String ip, long timestamp, int nextId) {
		return String.format("%s%s%s%s", getPID(), ip, timestamp, nextId);
	}

	private static String getPID() {
		String processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
		String PID = processName.split("@")[0];
		int length = PID.length();
		StringBuilder pidBuilder = new StringBuilder();
		for (int i = length; i < 5; i++) {
			pidBuilder.insert(0, '0');
		}
		return pidBuilder.toString();
	}

	private static int getNextId() {
		while (true) {
			int current = count.get();
			int next = (current > 9000) ? 1000 : current + 1;
			if (count.compareAndSet(current, next)) {
				return next;
			}
		}
	}

	/**
	 * 生成 链路id
	 */
	public static String generate() {
		return getTraceId(IP_16, System.currentTimeMillis(), getNextId());
	}

}
