package com.jiang.ddd.tool;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StopWatch;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Stop Watch工具类
 *
 * @author jiang.li
 * @since 2021/8/25
 */
@Slf4j
public class WatchTool {

    /**
     * 运行计时工具类，测试用的多
     *
     * @param runnableList 运行代码
     */
    public static void stopWatch(Runnable... runnableList) {
        StopWatch stopWatch = new StopWatch();
        int index = 1;
        for (Runnable runnable : runnableList) {
            stopWatch.start("Task" + index);
            runnable.run();
            stopWatch.stop();
            index++;
        }
        log.debug(stopWatch.prettyPrint());
    }


    /**
     * 自己的简易版 stopwatch ，可以持续start
     *
     * @author jiang.li
     * @since 2021/12/14
     */
    public static class WatchStop {
        private List<WatchNode> watchNodeList = new ArrayList<>();

        private boolean hasStoped = false;
        private int nowLevel = 0;

        public void start() {
            this.start("task" + (watchNodeList.size() + 1));
        }

        public void start(String taskName) {
            WatchNode watchNode = new WatchNode();
            watchNode.setStartTime(System.currentTimeMillis());
            nowLevel++;
            watchNode.setLevel(nowLevel);
            watchNode.setTaskName(taskName);
            watchNodeList.add(watchNode);
            hasStoped = false;
        }

        public void stop() {
            WatchNode watchNode = null;
            for (int i = watchNodeList.size() -1; i >= 0 ; i--) {
                watchNode = watchNodeList.get(i);
                if (watchNode.getEndTime() == 0){
                    watchNode.setEndTime(System.currentTimeMillis());
                    break;
                }
            }
            nowLevel--;
            // 计算百分比
            if (hasStoped) {
                int nowIndex = watchNodeList.size() - 1;
                List<WatchNode> processNodes = new ArrayList<>();
                while (nowIndex >= 0  && watchNodeList.get(nowIndex).getLevel() >= watchNode.getLevel()) {
                    if (Objects.equals(watchNodeList.get(nowIndex).getLevel(), watchNode.getLevel())) {
                        processNodes.add(watchNodeList.get(nowIndex));
                    }
                    nowIndex--;
                }
                long totalTime = processNodes.stream()
                        .mapToLong(wn -> wn.getEndTime() - wn.getStartTime())
                        .sum();
                processNodes.forEach(processNode -> {
                    long time = processNode.getEndTime() - processNode.getStartTime();
                    processNode.setPercent(Math.toIntExact(time / totalTime * 100));
                });
            }
            hasStoped = true;
        }

        public String prettyPrint() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("watch result：\n");

            for (WatchNode watchNode : watchNodeList) {
                // 处理未stop的任务
                if (Objects.equals(watchNode.getEndTime(), 0)) {
                    watchNode.setEndTime(System.currentTimeMillis());
                }

                for (int i = 0; i < watchNode.getLevel(); i++) {
                    stringBuilder.append("---");
                }
                stringBuilder.append(String.format("%s%% |  %s ms | %s\n", watchNode.getPercent(), watchNode.getEndTime() - watchNode.getStartTime(), watchNode.getTaskName()));
            }
            return stringBuilder.toString();
        }

        @AllArgsConstructor
        @NoArgsConstructor
        @Data
        private static class WatchNode {
            private long startTime;
            private long endTime;
            private int level;
            private String taskName;
            private int percent;
        }
    }
}
