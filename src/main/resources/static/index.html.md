# ddd-test
Version |  Update Time  | Status | Author |  Description
---|---|---|---|---
v2021-09-21 19:53:42|2021-09-21 19:53:42|auto|@root|Created by smart-doc



## 测试controller
### 测试接口
**URL:** http://localhost/test

**Type:** GET

**Author:** jiang.li

**Content-Type:** application/json; charset=utf-8

**Description:** 测试接口

**Query-parameters:**

Parameter | Type|Description|Required|Since
---|---|---|---|---
test|string|No comments found.|true|-

**Body-parameters:**

Parameter | Type|Description|Required|Since
---|---|---|---|---
id|int64|No comments found.|false|-
name|string|No comments found.|false|-
location|string|No comments found.|false|-

**Request-example:**
```
curl -X GET -H 'Content-Type: application/json; charset=utf-8' -i http://localhost/test?test=test --data '{
  "id": 71,
  "name": "耀杰.钱",
  "location": "n2qqbe"
}'
```

**Response-example:**
```
string
```

### 测试counter
**URL:** http://localhost/visit

**Type:** GET

**Author:** jiang.li

**Content-Type:** application/x-www-form-urlencoded;charset=utf-8

**Description:** 测试counter

**Request-example:**
```
curl -X GET -i http://localhost/visit
```

**Response-example:**
```
string
```

### 测试counter2
**URL:** http://localhost/visit2

**Type:** GET

**Author:** jiang.li

**Content-Type:** application/x-www-form-urlencoded;charset=utf-8

**Description:** 测试counter2

**Request-example:**
```
curl -X GET -i http://localhost/visit2
```

**Response-example:**
```
string
```

### 测试 3
**URL:** http://localhost/visit3

**Type:** GET

**Author:** jiang.li

**Content-Type:** application/x-www-form-urlencoded;charset=utf-8

**Description:** 测试 3

**Request-example:**
```
curl -X GET -i http://localhost/visit3
```

**Response-example:**
```
string
```

### 测试email
**URL:** http://localhost/email/send

**Type:** GET

**Author:** jiang.li

**Content-Type:** application/x-www-form-urlencoded;charset=utf-8

**Description:** 测试email

**Query-parameters:**

Parameter | Type|Description|Required|Since
---|---|---|---|---
content|string|内容|false|-

**Request-example:**
```
curl -X GET -i http://localhost/email/send?content=t3odz2
```

**Response-example:**
```
Doesn't return a value.
```

### 测试生成tranceId
**URL:** http://localhost/generate

**Type:** GET

**Author:** jiang.li

**Content-Type:** application/x-www-form-urlencoded;charset=utf-8

**Description:** 测试生成tranceId

**Request-example:**
```
curl -X GET -i http://localhost/generate
```

**Response-example:**
```
string
```

### 测试异常
**URL:** http://localhost/test/error

**Type:** GET

**Author:** jiang.li

**Content-Type:** application/x-www-form-urlencoded;charset=utf-8

**Description:** 测试异常

**Request-example:**
```
curl -X GET -i http://localhost/test/error
```

**Response-example:**
```
string
```

## test mock 前端控制器
### mock 测试
**URL:** http://localhost/mock-test/test

**Type:** GET

**Author:** jiang.li

**Content-Type:** application/x-www-form-urlencoded;charset=utf-8

**Description:** mock 测试

**Request-example:**
```
curl -X GET -i http://localhost/mock-test/test
```

**Response-example:**
```
string
```

## 错误码列表
Error code |Description
---|---
200|ok
400|Bad Request
401|Unauthorized
403|Forbidden
404|Not Found
415|Unsupported Media Type
500|Internal Server Error
502|Bad Gateway
503|Service Unavailable

