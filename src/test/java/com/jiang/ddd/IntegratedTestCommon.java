package com.jiang.ddd;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = ScanConfig.class)
@ActiveProfiles(value = "test")
public class IntegratedTestCommon {

}

@ComponentScan(excludeFilters = {})
class ScanConfig {

}