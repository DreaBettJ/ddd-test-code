package com.jiang.ddd

import com.alibaba.testable.core.tool.TestableTool
import spock.lang.Specification

class UnitTestCommon extends Specification {
    def setupSpec() {
        TestableTool.MOCK_CONTEXT = new HashMap<>()
    }
}
