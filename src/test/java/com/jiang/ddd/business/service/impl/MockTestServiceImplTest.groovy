package com.jiang.ddd.business.service.impl

import com.jiang.ddd.IntegratedTestCommon
import com.jiang.ddd.UnitTestCommon
import com.jiang.ddd.business.service.iservice.IMockTestService

import javax.annotation.Resource

class MockTestServiceImplTest extends UnitTestCommon {
    def "BatchUpdate"() {
    }
}

/**
 * 集成测试
 */
class app extends IntegratedTestCommon {
    @Resource
    IMockTestService mockTestService;

    def "BatchUpdate"() {
        given:

        expect:
            result == mockTestService.batchUpdate()
        where:
            result << [null]
    }
}
