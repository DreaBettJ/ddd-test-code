package com.jiang.ddd.chain;

import com.jiang.ddd.IntegratedTestCommon;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

public class ClientChainContainerTest extends IntegratedTestCommon {

	@Resource
	private ClientChainContainer clientChainContainer;

	@Test
	public void test() {
		List<ClientEnum> clientEnums = new ArrayList<>();
		clientEnums.add(ClientEnum.DING_DING);
		clientEnums.add(ClientEnum.EMAIL);
		ChainInput chainInput = new ChainInput(clientEnums, "test1", "test2");
		clientChainContainer.processRemind(chainInput);
	}

}