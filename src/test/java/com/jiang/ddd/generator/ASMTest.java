package com.jiang.ddd.generator;

import cn.hutool.core.io.IoUtil;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.junit.Test;
import org.springframework.asm.*;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import static org.springframework.asm.ClassReader.EXPAND_FRAMES;

public class ASMTest {

	@Test
	@SneakyThrows
	public void test() {
		String path = "D:\\Document\\Code\\ddd\\target\\test-classes\\com\\jiang\\ddd\\generator\\QDoxTest.class";
		@Cleanup
		InputStream inputStream = new FileInputStream(path);
		// 新建阅读IQ
		ClassReader classReader = new ClassReader(inputStream);
		classReader.accept(new MyClassVisitor(Opcodes.ASM8), EXPAND_FRAMES);
		ClassWriter classWriter = new ClassWriter(classReader, ClassWriter.COMPUTE_FRAMES);
		byte[] bytes = classWriter.toByteArray();
		@Cleanup
		FileOutputStream outputStream = new FileOutputStream(path);
		IoUtil.write(outputStream, true, bytes);
	}

	class MyClassVisitor extends ClassVisitor {

		public MyClassVisitor(int api) {
			super(api);
		}

		@Override
		public MethodVisitor visitMethod(int access, String name, String descriptor, String signature,
				String[] exceptions) {
			System.out.println("read method name:" + name);
			return new MethodVisitor(api) {
				@Override
				public void visitCode() {
					visitMethodInsn(Opcodes.INVOKESTATIC, "SecurityChecker", "checkSecurity", "()V");
					super.visitCode();
				}
			};
		}

	}

}
