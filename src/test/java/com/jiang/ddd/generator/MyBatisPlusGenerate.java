package com.jiang.ddd.generator;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.jiang.ddd.business.base.*;
import lombok.extern.slf4j.Slf4j;

/**
 * MyBatis plus 代码生成器
 *
 * @author lijiang
 * @since v1.0.0
 */
@Slf4j
public class MyBatisPlusGenerate {

	/**
	 * 作者
	 */
	private static final String AUTHOR = "jiang.li";

	/**
	 * 工作目录
	 */
	private static final String USER_DIR = System.getProperty("user.dir");

	/**
	 * JDBC 连接地址
	 */
	private static final String JDBC_URL = "jdbc:mysql://106.14.115.11:10002/mybatis-plus?characterEncoding=UTF-8&rewriteBatchedStatements=true";

	/**
	 * 数据库账号
	 */
	private static final String JDBC_USERNAME = "root";

	/**
	 * 数据库密码
	 */
	private static final String JDBC_PASSWORD = "123456";

	/**
	 * 包配置 - 父级目录
	 */
	private static final String PACKAGE_PARENT = "com.jiang.ddd.business.test";

	/**
	 * 包配置 - 模块目录 <br>
	 * 注意：如果表前缀与模块命相同，生成时会删除前缀，比如：core_admin 最终构建为 Admin, AdminController ...
	 */
	private static final String PACKAGE_MODULE_NAME = "";

	/**
	 * 包配置 - 实体类目录
	 */
	private static final String PACKAGE_ENTITY = "domain";

	/**
	 * 包配置 - 数据访问接口目录
	 */
	private static final String PACKAGE_MAPPER = "mapper";

	/**
	 * 包配置 - 业务处理接口目录
	 */
	private static final String PACKAGE_SERVICE = "service";

	/**
	 * 包配置 - 业务处理实现目录
	 */
	private static final String PACKAGE_SERVICE_IMPL = "service.impl";

	/**
	 * 包配置 - 控制器目录
	 */
	private static final String PACKAGE_CONTROLLER = "controller";

	/**
	 * 包配置 - 自定义目录
	 */
	private static final String PACKAGE_CUSTOM = "model";

	/**
	 * 要生成的表，用 `,` 分割
	 */
	private static final String[] TABLES = { "audit_record" };

	/**
	 * 模板路径
	 */
	private static final String TEMPLATE_PATH = "/my-templates";

	/**
	 * 全局配置
	 */
	private static void globalConfig(GlobalConfig.Builder builder) {
		builder.outputDir(USER_DIR + "/src/main/java").disableOpenDir().author(AUTHOR).build();
	}

	/**
	 * 包配置
	 */
	private static void packageConfig(PackageConfig.Builder builder) {
		builder.parent(PACKAGE_PARENT).moduleName(PACKAGE_MODULE_NAME).entity(PACKAGE_ENTITY).mapper(PACKAGE_MAPPER)
				.service(PACKAGE_SERVICE).serviceImpl(PACKAGE_SERVICE_IMPL).controller(PACKAGE_CONTROLLER)
				.other(PACKAGE_CUSTOM).build();
	}

	/**
	 * 代码生成模板配置 - Freemarker
	 */
	private static void templateConfig(TemplateConfig.Builder builder) {
		builder.entity(TEMPLATE_PATH + "/entity.java").mapper(TEMPLATE_PATH + "/mapper.java")
				.service(TEMPLATE_PATH + "/service.java").serviceImpl(TEMPLATE_PATH + "/serviceImpl.java")
				.controller(TEMPLATE_PATH + "/controller.java").build();
	}

	/**
	 * 代码生成策略配置
	 */
	private static void strategyConfig(StrategyConfig.Builder builder) {
		builder.addInclude(TABLES).addTablePrefix(PACKAGE_MODULE_NAME + "_").entityBuilder()
				.naming(NamingStrategy.underline_to_camel).columnNaming(NamingStrategy.underline_to_camel)
				.enableLombok().superClass(BasicEntity.class).controllerBuilder().enableRestStyle()
				.superClass(BasicController.class).mapperBuilder().superClass(BasicMapper.class).serviceBuilder()
				.superServiceClass(BasicService.class).superServiceImplClass(BasicServiceImpl.class).build();
	}

	/**
	 * 自定义配置 将mapper输出到resource下面
	 */
	private static void injectionConfig(InjectionConfig.Builder builder) {
		builder.beforeOutputFile((tableInfo, stringObjectMap) -> {
			log.info("config map:" + JSONUtil.toJsonStr(stringObjectMap));
			log.error("tableInfo:" + JSONUtil.toJsonStr(tableInfo));
		}).build();
	}

	public static void main(String[] args) {
		FastAutoGenerator.create(JDBC_URL, JDBC_USERNAME, JDBC_PASSWORD).globalConfig(MyBatisPlusGenerate::globalConfig)
				.packageConfig(MyBatisPlusGenerate::packageConfig).strategyConfig(MyBatisPlusGenerate::strategyConfig)
				.injectionConfig(MyBatisPlusGenerate::injectionConfig)
				.templateConfig(MyBatisPlusGenerate::templateConfig).execute();
	}

}
