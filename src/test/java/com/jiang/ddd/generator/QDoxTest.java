package com.jiang.ddd.generator;

import com.jiang.ddd.tool.NullTool;
import com.thoughtworks.qdox.JavaProjectBuilder;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaMethod;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class QDoxTest {

	@Test
	@SneakyThrows
	public void test() {
		long before = System.currentTimeMillis();
		JavaProjectBuilder javaProjectBuilder = new JavaProjectBuilder();
		javaProjectBuilder
				.addSourceTree(new File("D:\\Document\\Code\\ddd\\src\\main\\java\\com\\jiang\\ddd\\business"));
		Collection<JavaClass> classes = javaProjectBuilder.getClasses();
		for (JavaClass javaClass : classes) {
			// 打印类相关信息
			System.out.println("======================");
			System.out.println("类名:" + javaClass.getName());
			System.out.println("实现了哪些类：" + javaClass.getImplements());
			System.out.println("继承哪个类：" + javaClass.getSuperJavaClass());
			System.out.println("注释：" + javaClass.getComment());
			// 获得方法列表
			List<JavaMethod> methods = javaClass.getMethods();
			for (JavaMethod method : methods) {
				if (Objects.nonNull(method)) {
					System.out.println("--------------------");
					System.out.println("---代码体：" + method.getSourceCode());
					System.out.println("---方法名是：" + method.getName());
					System.out.println("---方法的 Tags 有哪些：" + NullTool.stream(method::getTags)
							.map(it -> it.getName() + "->" + it.getValue()).collect(Collectors.joining("\n")));
					System.out.println("---方法的参数有哪些：" + method.getParameters());
					System.out.println("---方法的返回值有哪些：" + method.getReturns());
					System.out.println("----------------------");
				}
			}

			System.out.println("======================");
		}
		long process = System.currentTimeMillis() - before;
		System.out.println("方法耗时：" + process + "ms");
	}

}
