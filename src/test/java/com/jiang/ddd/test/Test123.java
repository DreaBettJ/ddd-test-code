package com.jiang.ddd.test;

import com.jiang.ddd.tool.JacksonTool;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;

public class Test123 {

    RestTemplate restTemplate = new RestTemplate();

    @Test
    public void test() {

        String url = "http://47.103.102.145:10051/hlyfpcy";
        // 请求头
        MultiValueMap<String, String> header = new LinkedMultiValueMap<>();
        header.add(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE);
        header.add(HttpHeaders.AUTHORIZATION, "Basic MTUxNDAxMzQ2NzYxMzE3NjMxUkM6OGVhZWZjMDNhZjE4ZTg4NWJkYmM3YmUwMjkzYWZlZjY=");
        header.add(HttpHeaders.ACCEPT_ENCODING, StandardCharsets.UTF_8.displayName());
        //{
        //    "cd": {
        //        "sv": "cd",
        //        "fpdm": "6100203130",
        //        "fphm": "13935379",
        //        "kprq": "20210322",
        //        "value": "67284.96",
        //        "type": "0"
        //    }
        //}
        Wrapper wrapper = new Wrapper();
        CD cd = CD.builder()
                .sv("cd")
                .fpdm("3100164320")
                .fphm("59957351")
                .kprq("20190306")
                .value("315048")
                .type("1")
                .build();

        wrapper.setCd(cd);

//        Map<String, Object> body = new HashMap<>();
//        Map<String, String> kvMap = new HashMap<>();
//        kvMap.put("sv","cd");
//        kvMap.put("fpdm","6100203130");
//        kvMap.put("fphm","13935379");
//        kvMap.put("kprq","20210322");
//        kvMap.put("value","67284");
//        kvMap.put("type","0");
//        body.put("cd",kvMap);


        HttpEntity<Object> httpEntity = new HttpEntity<>(JacksonTool.obj2XMLStr(cd), header);

        ResponseEntity<String> stringResponseEntity = restTemplate.postForEntity(url, httpEntity, String.class);

        System.out.println(stringResponseEntity.getBody());
    }


    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    static class Wrapper{
        CD cd;
    }

    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    static class CD{
        private String sv;
        private String fpdm;
        private String fphm;
        private String kprq;
        private String value;
        private String type;
    }
}
