package com.jiang.ddd.tool

import com.jiang.ddd.business.model.MockTestModel
import spock.lang.Specification

class ReflectInfoToolTest extends Specification {
    def "GetActualTypeArgument"() {
        given:
        def clazz = reflectTool.getClassActualType(MockTestModel.class)
        expect:
        clazz != null
    }
}
